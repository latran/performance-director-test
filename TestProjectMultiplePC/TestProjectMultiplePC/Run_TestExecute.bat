REM Clears the screen
CLS
ECHO OFF

REM Setup number PC
echo 4 > C:\SST\setup.txt

REM Launches TestExecute
REM executes the specified project
REM and closes TestExecute when the run is over
start TestExecute.exe /r /e "S:\@QA\@AutomaionServer\TestComplete\TestProjectMultiplePC\TestProjectMultiplePC.pjs"


IF ERRORLEVEL 1001 GOTO NotEnoughDiskSpace
IF ERRORLEVEL 1000 GOTO AnotherInstance
IF ERRORLEVEL 127 GOTO DamagedInstall
IF ERRORLEVEL 4 GOTO Timeout
IF ERRORLEVEL 3 GOTO CannotRun
IF ERRORLEVEL 2 GOTO Errors
IF ERRORLEVEL 1 GOTO Warnings
IF ERRORLEVEL 0 GOTO Success
IF ERRORLEVEL -1 GOTO LicenseFailed
 
:NotEnoughDiskSpace
ECHO There is not enough free disk space to run TestExecute
GOTO End
 
:AnotherInstance
ECHO Another instance of TestExecute is already running
GOTO End
 
:DamagedInstall
ECHO TestExecute installation is damaged or some files are missing
GOTO End
 
:Timeout
ECHO Timeout elapsed
GOTO End
 
:CannotRun
ECHO The script cannot be run
GOTO End
 
:Errors
ECHO There are errors
GOTO End
 
:Warnings
ECHO There are warnings
GOTO End
 
:Success
ECHO No errors
GOTO End
 
:LicenseFailed
ECHO License check failed
GOTO End

:End