﻿function GeneralEvents_OnUnexpectedWindow(Sender, Window, LogParams)
{
       LogParams.Locked = true;

  // Post an error message about unexpected windows to the test log
  Log.Error("Unexpected window");
 
  // Post an image of the whole screen to the test log
  Log.Picture(Window.Picture(), "Image of the entire screen", "This image was added to the test log from the GeneralEvents_OnUnexpectedWindow procedure");

    if (Aliases.TrussStudio.dlgComponentSolutionsTrussStudio.Exists)
    {
    Aliases.TrussStudio.dlgComponentSolutionsTrussStudio.btnOK.ClickButton();
    }
    else
    {
    // Close the unexpected window
    Window.Close();
    }

}