﻿//USEUNIT UtilitiesForApps 
var FormMain = UserForms.FormTS_AUTOMATION;

function Main (StrMachine)
{
    LoginDirector(StrMachine);
    RunMain(StrMachine);
}
function RunMain (StrMachine)
{
 
 ResizeForm();
 
 Project.Variables.PathDataFile = Project.Path+"DataFile.txt";
 if(!aqFile.Exists(Project.Variables.PathDataFile))
 {
  WriteTextToFile(Project.Variables.PathDataFile,1,"processing");
 }
  
 if(StrMachine == "1")
 {
  WriteTextToFile(Project.Variables.PathDataFile,1,"processing");
  Machine1(StrMachine);
 }
 if (StrMachine=="2")
 {
  Machine2(StrMachine);
 }
 if (StrMachine=="3")
 {
  Machine3(StrMachine);
 }


}

function Machine1 (StrMachine)
{
  var CheckInButton = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonSystemGroup.CheckInButton;
  var btnAdd = Aliases.CSDirector.HwndSource_HandleCheckin.HandleCheckin.btnAdd;

  var RadTreeViewItem = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer.NavigatorRadPanelGroup.NavTreeView.Administration.Employees.RadTreeViewItem;
  
  
  var refreshProjects = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonSystemGroup.refreshProjects;
  var DeleteProject = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonProjectGroup.DeleteProject;
  var CheckOutButton = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonSystemGroup.CheckOutButton;
  var rdCheckOutProjectForEdit = Aliases.CSDirector.HwndSource_HandleCheckout.HandleCheckout.rdCheckOutProjectForEdit;
  var CheckInButton = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonSystemGroup.CheckInButton;
  var btnAdd = Aliases.CSDirector.HwndSource_HandleCheckin.HandleCheckin.btnAdd;
  
  var ListviewitemProjectList = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.LvwOutlookMenu.ListviewitemProjectList;
  
  //step 1
  CreateNewProject("Test Case 1a");
  aqUtils.Delay(1000);
  CheckInButton.Click();
  btnAdd.ClickButton();
  
  WriteTextToFile(Project.Variables.PathDataFile,2,"processing");
  WaitMachine("1");
  
  //Step 2
  aqUtils.Delay(3000);
  refreshProjects.Click();
  aqUtils.Delay(3000);
  Aliases.CSDirector.HwndSource_CSCommandApplication.DblClick(500,280);
  rdCheckOutProjectForEdit.ClickButton();
  Aliases.CSDirector.HwndSource_HandleCheckout.HandleCheckout.btnOk.ClickButton();
    
  CreateNewProject("Test Case 1b");
  aqUtils.Delay(4000);
  CheckInButton.Click();
  btnAdd.ClickButton();
  
  WriteTextToFile(Project.Variables.PathDataFile,3,"processing");
  WaitMachine("1");
  
  RadTreeViewItem.DblClick();
  
  AddNote();
  CheckInButton.Click();
  btnAdd.ClickButton();
  WriteTextToFile(Project.Variables.PathDataFile,1,"end");
  
}

function Machine2(StrMachine)
{

  var refreshProjects = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonSystemGroup.refreshProjects;
  var DeleteProject = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonProjectGroup.DeleteProject;
  var CheckOutButton = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonSystemGroup.CheckOutButton;
  var rdCheckOutProjectForEdit = Aliases.CSDirector.HwndSource_HandleCheckout.HandleCheckout.rdCheckOutProjectForEdit;
  var CheckInButton = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonSystemGroup.CheckInButton;
  var btnAdd = Aliases.CSDirector.HwndSource_HandleCheckin.HandleCheckin.btnAdd;
  
  var ListviewitemProjectList = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.LvwOutlookMenu.ListviewitemProjectList;

  //step 2 
  WaitMachine("2");
  aqUtils.Delay(3000);
  refreshProjects.Click();
  aqUtils.Delay(3000);
  Aliases.CSDirector.HwndSource_CSCommandApplication.Click(500,280);
  while (!DeleteProject.Enabled)
  {
  aqUtils.Delay(3000);
  refreshProjects.Click();
  aqUtils.Delay(3000);
  Aliases.CSDirector.HwndSource_CSCommandApplication.Click(500,280);
  }
  Aliases.CSDirector.HwndSource_CSCommandApplication.DblClick(500,280);
  rdCheckOutProjectForEdit.ClickButton();
  Aliases.CSDirector.HwndSource_HandleCheckout.HandleCheckout.btnOk.ClickButton();
  
  RunTrussStudio();
  ListviewitemProjectList.Click();
  
  CheckInButton.Click();
  btnAdd.ClickButton();
  
  WriteTextToFile(Project.Variables.PathDataFile,1,"processing");
  WaitMachine("2");
  
  //Step 3
  aqUtils.Delay(3000);
  refreshProjects.Click();
  aqUtils.Delay(3000);
  Aliases.CSDirector.HwndSource_CSCommandApplication.Click(500,280);
  while (!DeleteProject.Enabled)
  {
  aqUtils.Delay(3000);
  refreshProjects.Click();
  aqUtils.Delay(3000);
  Aliases.CSDirector.HwndSource_CSCommandApplication.Click(500,280);
  }
  Aliases.CSDirector.HwndSource_CSCommandApplication.DblClick(500,280);
  rdCheckOutProjectForEdit.ClickButton();
  Aliases.CSDirector.HwndSource_HandleCheckout.HandleCheckout.btnOk.ClickButton();
  
  CreateTruss();
  
  aqUtils.Delay(3000);
  
  ListviewitemProjectList.Click();
  
  CheckInButton.Click();
  btnAdd.ClickButton();
  
  WriteTextToFile(Project.Variables.PathDataFile,1,"processing");
  WaitMachine("2");
  
}

function Machine3(StrMachine)
{

  var refreshProjects = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonSystemGroup.refreshProjects;
  var DeleteProject = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonProjectGroup.DeleteProject;
  var CheckOutButton = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonSystemGroup.CheckOutButton;
  var rdCheckOutProjectForEdit = Aliases.CSDirector.HwndSource_HandleCheckout.HandleCheckout.rdCheckOutProjectForEdit;
  var CheckInButton = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonSystemGroup.CheckInButton;
  var btnAdd = Aliases.CSDirector.HwndSource_HandleCheckin.HandleCheckin.btnAdd;
  
  var ListviewitemProjectList = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.LvwOutlookMenu.ListviewitemProjectList;

  //step 3
  WaitMachine("3");
  aqUtils.Delay(3000);
  refreshProjects.Click();
  aqUtils.Delay(3000);
  Aliases.CSDirector.HwndSource_CSCommandApplication.Click(500,280);
  while (!DeleteProject.Enabled)
  {
  aqUtils.Delay(3000);
  refreshProjects.Click();
  aqUtils.Delay(3000);
  Aliases.CSDirector.HwndSource_CSCommandApplication.Click(500,280);
  }
  Aliases.CSDirector.HwndSource_CSCommandApplication.DblClick(500,280);
  rdCheckOutProjectForEdit.ClickButton();
  Aliases.CSDirector.HwndSource_HandleCheckout.HandleCheckout.btnOk.ClickButton();
  
  CheckInButton.Click();
  btnAdd.ClickButton();
  
  WriteTextToFile(Project.Variables.PathDataFile,2,"processing");
  WaitMachine("3");
  
  
}

function WaitMachine (arg)
{
var Arr = ODT.Classes.NewArray();
var i=1;

var refreshProjects = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonSystemGroup.refreshProjects;



Arr=ReadDataFromFile(Project.Variables.PathDataFile);
Log.PushLogFolder(Log.CreateFolder("Refresh project")); 

 while(Arr.Items(0)!=arg)
 {
  i++;
  aqUtils.Delay(1000);
  if (i>120)
  {
    if (Arr.Items(1)!="end")
    {
      i=1;
    }
    else
    {
      Runner.Stop();
    }
  }
  if (Arr.Items(1)=="end")
  {
      Runner.Stop();
  }
  if (aqFile.Exists(Project.Variables.PathDataFile))
  {
    Arr=ReadDataFromFile(Project.Variables.PathDataFile);
  }
  if (i%2==0)
  {
    refreshProjects.Click();
  }
  
 }  
Log.PopLogFolder();
}

function AddNote()
{
  var RadtreeviewitemProjectInfo = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.RadTreeMenu.RadtreeviewitemProjectInfo;
  var AttachmentNotesItem = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.radTabControlProjectInfo.AttachmentNotesItem;
  var DocumentWebLayoutPresenter = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.radTabControlProjectInfo.CustomerNote.DocumentWebLayoutPresenter;
    
  RadtreeviewitemProjectInfo.Click();
  AttachmentNotesItem.Click();
  DocumentWebLayoutPresenter.Keys("note for testing");
  
}

function CreateTruss()
{
  var RadtreeviewitemComponents = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.RadTreeMenu.RadtreeviewitemComponents;
  var AddTruss = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonComponentsTrussesGroup.AddTruss;
  var txtNewUniqueTrussName = Aliases.CSDirector.HwndSource_TrussNameDialog.TrussNameDialog.txtNewUniqueTrussName;
  
  var editSpan = Aliases.TrussStudio.FormTS.Item.wndAfx.AfxMDIFrame120u.page32770.editSpan;
  var editTCProfile = Aliases.TrussStudio.FormTS.Item.wndAfx.AfxMDIFrame120u.page32770.editTCProfile;
  var btnRun = Aliases.TrussStudio.FormTS.Item.wndAfx.AfxMDIFrame120u.page32770.btnRun;
  
  RadtreeviewitemComponents.Click();
  AddTruss.Click(10,10);
  txtNewUniqueTrussName.SetText("T1");
  Aliases.CSDirector.HwndSource_TrussNameDialog.TrussNameDialog.RadbuttonOk.ClickButton();
  
  var FormTS = Aliases.TrussStudio.FormTS;
  
  UtilitiesForApps.ResizeFormTS();
  
aqUtils.Delay(5000); 
   
editSpan.SetText("20");
editTCProfile.SetText("6");
 btnRun.ClickButton();
 
 aqUtils.Delay(2000);
 FormTS.Keys("^s[Release]");
 aqUtils.Delay(2000);
FormTS.Close(); 
  
}

function CreateNewProject(strProj)
{
 var ListviewitemProjectList = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.LvwOutlookMenu.ListviewitemProjectList;
 var CreateProjectDropDown = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonProjectGroup.CreateProjectDropDown;
 var ProjectDescription = Aliases.CSDirector.HwndSource_winCreateProject.winCreateProject.ProjectDescription;
 var TemplatesComboBox = Aliases.CSDirector.HwndSource_winCreateProject.winCreateProject.TemplatesComboBox;
 var ProjectMaterialCatalogCombo = Aliases.CSDirector.HwndSource_winCreateProject.winCreateProject.ProjectMaterialCatalogCombo;
 
 var CreateProjectButton = Aliases.CSDirector.HwndSource_winCreateProject.winCreateProject.CreateProjectButton;
 
 ListviewitemProjectList.Click();
 CreateProjectDropDown.Click(10,10);
 ProjectDescription.SetText(strProj);
 //ProjectMaterialCatalogCombo.ClickItem(1);//1 = Smoke Test
 //TemplatesComboBox.ClickItem("Studio");
 
 CreateProjectButton.ClickButton();
 
}
function RunTrussStudio ()
{
 var RadtreeviewitemPlansElevations = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.RadTreeMenu.RadtreeviewitemPlansElevations;
 var RadtreeviewitemElevationA = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.PlansRadTreeView.RadTreeViewItem.RadtreeviewitemElevationA;
 
 var RunTitusBuild = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonElevationItems.RunTitusBuild;
 
 aqUtils.Delay(3000);
 
 RadtreeviewitemPlansElevations.Click();
 
 RadtreeviewitemElevationA.Click();
 RunTitusBuild.Click();
 
 aqUtils.Delay(5000);
 // Truss Studio
 CreateLayout();
 
}

function CreateLayout ()
{
  var FormTrussStudio = Aliases.TrussStudio.FormTrussStudio;
  var editLayoutComm = Aliases.TrussStudio.FormTrussStudio.MDIClient.wndAfx.AfxMDIFrame120u.page32770.editLayoutComm;
 
  while (!FormTrussStudio.Exists)
  {
    
  }
  
   UtilitiesForApps.ResizeFormTruss();
   aqUtils.Delay(1000);
   UtilitiesForApps.ResetPositionToolbarTruss();
   
   editLayoutComm.SetText("u20");editLayoutComm.Keys("[End][Enter][Release]");
   editLayoutComm.SetText("r40");editLayoutComm.Keys("[End][Enter][Release]");
   editLayoutComm.SetText("d20");editLayoutComm.Keys("[End][Enter][Release]");
   editLayoutComm.SetText("c");editLayoutComm.Keys("[End][Enter][Release]");
   
  
  FormTrussStudio.Keys("^s[Release]");
  
  FormTrussStudio.Keys("[Alt][Down]x");
  
  if (Aliases.TrussStudio.dlgComponentSolutionsTrussStudio.Exists)
  {
    Aliases.TrussStudio.dlgComponentSolutionsTrussStudio.btnYes.ClickButton();
  }
  
   
}


//*********************************************************

function LoginDirector (StrMachine)
{
  Aliases.CSDirector.HwndSource_winLoginDialog
  var editPassword = Aliases.CSDirector.HwndSource_winLoginDialog.winLoginDialog.Password;
  var editUsername = Aliases.CSDirector.HwndSource_winLoginDialog.winLoginDialog.Username;
  var BtnOk = Aliases.CSDirector.HwndSource_winLoginDialog.winLoginDialog.Okay;
  
  var Fsus = false;
  

    if (WaitWin(Aliases.CSDirector.HwndSource_winLoginDialog))
    {
      if (StrMachine=="1")
      {
        editUsername.SetText("admin");
        editPassword.Keys("admin");
      }
      if (StrMachine=="2")
      {
				WaitMachine()
			
        editUsername.SetText("client1");
        editPassword.Keys("client1");
      }
      if (StrMachine=="3")
      {
        editUsername.SetText("client2");
        editPassword.Keys("client2");
      }
      BtnOk.Click();
      Fsus = true ;
    } 

  
  return Fsus;
}
