﻿//USEUNIT UtilitiesForApps 
var FormMain = UserForms.FormTS_AUTOMATION;

function Main (ListFtdltxt)
{
    var PathVersion = aqString.SubString(Project.Variables.FilePartTrussStudio,0,aqString.FindLast(Project.Variables.FilePartTrussStudio,"TrussStudio"));
    var NameVersion = getNameVersion(PathVersion);
  //*** "C:\\SST\\Local\\";
    //PreTest(NameVersion);
    
    //RunCreate(NameVersion);
    RunMain(ListFtdltxt);
    
    
}
function getNameVersion (PathVersion)
{
  var NameVersion = aqString.SubString(PathVersion,0,aqString.FindLast(PathVersion,"\\"));
  NameVersion = aqString.SubString(NameVersion,aqString.FindLast(NameVersion,"\\")+1,aqString.GetLength(NameVersion)-aqString.FindLast(NameVersion,"\\"));
  return NameVersion;
}
function ReadFile ()
{

var PathTXT = Project.Variables.FilePathTdl;
var ListFtxt = ODT.Classes.NewArray();
var StrtTemp ="";

  var myFile = aqFile.OpenTextFile(PathTXT, aqFile.faRead, aqFile.ctANSI);
    while(! myFile.IsEndOfFile())
  {
    StrtTemp =myFile.ReadLine();
    if (aqString.Find(StrtTemp,"</Script>") == -1 )
    {
      ListFtxt.AddItem(StrtTemp);
    }
    else 
    {
      break;
    }    
  }
return ListFtxt;


}

function RunMain (ListFtdltxt)
{

  var ListFtxt = ODT.Classes.NewArray();

  var FormTS = Aliases.TrussStudio.FormTS ;  
  var WinScript = Aliases.TrussStudio.FormTS.Script.WinScript;
  var tbLayoutCommands = Aliases.TrussStudio.FormTS.MDIClient.wndAfx.AfxMDIFrame120u.page32770.editTCProfile;
  var StrTemp = "";
  var boolrun = false;
  
  //UtilitiesForApps.ResizeForm();
  //UtilitiesForApps.ResetPositionToolbar();
  
  ListFtxt = ListFtdltxt ;
  
  FormTS.Keys("^l[Release]");
  
  Aliases.TrussStudio.FormTS.MDIClient.wndAfx.AfxMDIFrame120u.AfxFrameOrView120u.Click(886, 70);
  
  //WinScript.SetText("");
  //WinScript.Keys("[Enter]");
  
  for (i=Project.Variables.tbLineFrom -1 ; i < Project.Variables.tbLineTo ;i++)
  {
   if (!boolrun)
   {
    if (aqString.GetLength(ListFtxt.Items(i))<10)
    {
      boolrun=true;
    }
   }
    
   if (boolrun)
   {
    StrTemp +=  ListFtxt.Items(i)+ "\n" ;
    if (i % aqConvert.StrToInt(Project.Variables.NumLine) ==0 )
    {
      tbLayoutCommands.SetText(StrTemp);
      tbLayoutCommands.Keys("[End][Release]");
      tbLayoutCommands.Keys("[Enter][Release]");
      Log.Picture(FormTS,"NumberLine :"+(i-aqConvert.StrToInt(Project.Variables.NumLine))+" - "+ i +" \ Content :"+StrTemp);
      StrTemp="";
    }
    else if (i == Project.Variables.tbLineTo - 1)
    {
      tbLayoutCommands.SetText(StrTemp);
      tbLayoutCommands.Keys("[End][Release]");
      tbLayoutCommands.Keys("[Enter][Release]");
      Log.Picture(FormTS,"NumberLine :"+i-(i % aqConvert.StrToInt(Project.Variables.NumLine))+ " - " + i + " \ Content :"+StrTemp);
    }
   }
  }
  


}

function RunCreate(NameVersion)
{
  var FormTS = Aliases.TrussStudio.FormTS ;
  var EditFileName = Aliases.TrussStudio.dlgSaveAs.DUIViewWndClassName.Explorer_Pane.FloatNotifySink.ComboBox;
  
    
  
  var driver = DDT.ExcelDriver(ProjectSuite.Path+"Template\\DataCreateTruss.xlsx","Standard Condition",true);
  var i = 0
  var a =1;
  var m1 ="";
  var FlagStop = false;
  
//**********Get value form Excel - Read Data.
//Begin test

  while (!driver.EOF())
  { 
    if (a<6) 
      FlagStop =true;
    else  
      FlagStop =false;
  if (driver.Value(0)!=null)
  {
    if (FlagStop==false)
    {
    
    
    }
  }
    a++;
    
    DDT.CurrentDriver.Next();
  }
  DDT.CloseDriver(driver.Name);


}
//*********



function OpenTruss(PartTS)
{
  var FormTS = Aliases.TrussStudio.FormTS;
  //var EditFileName = Aliases.TRUSSS_1.dlgOpen.ComboBoxEx32.ComboBox.EditFileName;
  var EditFileName = Aliases.TrussStudio.dlgOpen.ComboBoxEx32.ComboBox.EditFileName;
  var btnOpen = Aliases.TrussStudio.dlgOpen.btnOpen;
  FormTS.Keys("^o[Release]");
  EditFileName.SetText(PartTS+"JOBS\\GirderSetback\\Cal.tdlLayout");
  btnOpen.ClickButton();
  //C:\SST\Local\JOBS\CalHipModsToHipSetDialog\Hip001.tdlLayout
}

function PreTest(NameVersion)
{ 
  
  aqFileSystem.DeleteFolder("C:\\TrussBank\\"+NameVersion,true);
  aqFileSystem.CreateFolder("C:\\TrussBank\\"+NameVersion);
//  aqFileSystem.DeleteFolder("S:\\@QA\\VNQA_BuildOutput\\TestComplete Data\\AtticTruss\\"+NameVersion,true);
//  aqFileSystem.CreateFolder("S:\\@QA\\VNQA_BuildOutput\\TestComplete Data\\AtticTruss\\"+NameVersion);
  

//    //PathTS = "C:\\SST\\Local\\TrussStudio";// -> PathVersion = "C:\\SST\\Local\\"
//      
//    aqFileSystem.DeleteFolder(PartTS+"JOBS",true);
//    aqFileSystem.CreateFolder(PartTS+"JOBS");
//    aqFileSystem.CopyFolder(ProjectSuite.Path+"Template\\GirderSetback",PartTS+"JOBS",false);   
}
function CloseTruss ()
{
aqUtils.Delay(500);
  var FormTS = Aliases.TrussStudio.FormTS;
  FormTS.Keys("[Alt][Enter]c[Release]");
  //UtilitiesForApps.ClickBtntruss(2);
  
}
