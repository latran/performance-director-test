﻿//USEUNIT UtilitiesForApps 
var FormMain = UserForms.FormTS_AUTOMATION;

function Main ()
{
    LoginDirector();
    RunMain();
}

function RunMain ()
{
  
  
  //var Presets = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonSettingsItems.Presets;
  //var SawTables = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonSettingsItems.SawTables;
  var MaterialCatalogs = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonSettingsItems.MaterialCatalogs;
  var Presets = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonSettingsItems.Presets;
  
  ResizeForm();

  //RadribbontabTools.Click();
  
  
    // ************BEGIN
  var RadribbontabTools = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RadribbontabTools;
  
  RadribbontabTools.Click(5,5);
  if (!Presets.Exists)
  {
  RadribbontabTools.Click(5,5);
  }
  
  
  SettingPreset();
  SettingSawTable();
  SettingMaterialCatalog();
  SettingAdministration();
  
  CreateNewProject();

//  CreateProject();
//  SetupProject();
//  
//  //Create Project from DDP
//  CreateProjectAssembly();
//  SetPlanElevationsAssemply();
//  SetBatchProcessAssemply();
  
}
//********Create new Project************
//***http://testrail-ser1.simpsonmfg.com/testrail/index.php?/cases/view/77374****


function CreateNewProject()
{
 var ListviewitemProjectList = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.LvwOutlookMenu.ListviewitemProjectList;
 var CreateProjectDropDown = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonProjectGroup.CreateProjectDropDown;
 var ProjectDescription = Aliases.CSDirector.HwndSource_winCreateProject.winCreateProject.ProjectDescription;
 var TemplatesComboBox = Aliases.CSDirector.HwndSource_winCreateProject.winCreateProject.TemplatesComboBox;
 var ProjectMaterialCatalogCombo = Aliases.CSDirector.HwndSource_winCreateProject.winCreateProject.ProjectMaterialCatalogCombo;
 
 var CreateProjectButton = Aliases.CSDirector.HwndSource_winCreateProject.winCreateProject.CreateProjectButton;
 
 var RadtreeviewitemPlansElevations = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.RadTreeMenu.RadtreeviewitemPlansElevations;
 var RadtreeviewitemElevationA = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.PlansRadTreeView.RadTreeViewItem.RadtreeviewitemElevationA;
 
 var RunTitusBuild = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonElevationItems.RunTitusBuild;
 
 ListviewitemProjectList.Click();
 CreateProjectDropDown.Click(10,10);
 ProjectDescription.SetText("Studio");
 ProjectMaterialCatalogCombo.ClickItem(1);//1 = Smoke Test
 TemplatesComboBox.ClickItem("Studio");
 
 CreateProjectButton.ClickButton();
 
 aqUtils.Delay(3000);
 
 RadtreeviewitemPlansElevations.Click();
 
 RadtreeviewitemElevationA.Click();
 RunTitusBuild.Click();
 
 aqUtils.Delay(5000);
 // Truss Studio
 RunTrussStudio();

 
}


function RunTrussStudio ()
{
  var FormTrussStudio = Aliases.TrussStudio.FormTrussStudio;
  var editLayoutComm = Aliases.TrussStudio.FormTrussStudio.MDIClient.wndAfx.AfxMDIFrame120u.page32770.editLayoutComm;
 
  while (!FormTrussStudio.Exists)
  {
    
  }
  
   UtilitiesForApps.ResizeFormTruss();
   aqUtils.Delay(1000);
   UtilitiesForApps.ResetPositionToolbarTruss();
   
   editLayoutComm.SetText("u20");editLayoutComm.Keys("[End][Enter][Release]");
   editLayoutComm.SetText("r40");editLayoutComm.Keys("[End][Enter][Release]");
   editLayoutComm.SetText("d20");editLayoutComm.Keys("[End][Enter][Release]");
   editLayoutComm.SetText("c");editLayoutComm.Keys("[End][Enter][Release]");
   
  var afxFrameOrView120u;
  afxFrameOrView120u = Aliases.TrussStudio.FormTrussStudio.MDIClient.wndAfx.AfxMDIFrame120u.AfxFrameOrView120u;
  afxFrameOrView120u.Click(346, 221);
  
  
  var trussStudio;
  trussStudio = Aliases.TrussStudio;
  trussStudio.FormTrussStudio.BCGPDockBarc0000081000510.toolbarMenuBar.ClickItem(2, true);
  trussStudio.wndAfx.Item.CheckItem("Properties Window", true, false);
  
  aqUtils.Delay(1000);
  
  FormTrussStudio.Keys("[Down][Down][Down][Down][Down][Down][Down][Down][Down][Down][Down][Down][Down][Down][Down][Down][Down][Down][Down][Down][Down]None[Enter]");
  
  FormTrussStudio.Keys("[Esc][Release]");    
  FormTrussStudio.Keys("~r[Release]");
  var afxFrameOrView120u;
  afxFrameOrView120u = Aliases.TrussStudio.FormTrussStudio.MDIClient.wndAfx.AfxMDIFrame120u.AfxFrameOrView120u;
  afxFrameOrView120u.Click(788, 133);
  afxFrameOrView120u.Click(955, 325);
  afxFrameOrView120u.Click(575, 419);
  afxFrameOrView120u.Click(312, 183);
  afxFrameOrView120u.ClickR(303, 186);
  
  FormTrussStudio.Keys("[Esc][Release]");    
  FormTrussStudio.Keys("~r[Release]");
  var afxFrameOrView120u;
  afxFrameOrView120u = Aliases.TrussStudio.FormTrussStudio.MDIClient.wndAfx.AfxMDIFrame120u.AfxFrameOrView120u;
  afxFrameOrView120u.Click(967, 334);
  afxFrameOrView120u.Click(736, 421);
  afxFrameOrView120u.Click(743, 138);
  afxFrameOrView120u.ClickR(743, 137);
  
  FormTrussStudio.Keys("[Esc][Release]");    
  FormTrussStudio.Keys("~r[Release]");
  var afxFrameOrView120u;
  afxFrameOrView120u = Aliases.TrussStudio.FormTrussStudio.MDIClient.wndAfx.AfxMDIFrame120u.AfxFrameOrView120u;
  afxFrameOrView120u.Click(700, 411);
  afxFrameOrView120u.Click(313, 323);
  afxFrameOrView120u.Click(778, 140);
  afxFrameOrView120u.Click(956, 355);
  afxFrameOrView120u.ClickR(956, 355);
  
  FormTrussStudio.Keys("[Esc][Release]");
  
  var TrussStudio;
  TrussStudio = Aliases.TrussStudio;
  TrussStudio.FormTrussStudio.BCGPDockBarc0000081000510.toolbarMenuBar.ClickItem(7, true);
  TrussStudio.wndAfx.Item.CheckItem("&Hip Set", true, false);
  
  Aliases.TrussStudio.dlgHipSetDialog.btnOK.ClickButton();
  
    var afxFrameOrView120u;
  afxFrameOrView120u = Aliases.TrussStudio.FormTrussStudio.MDIClient.wndAfx.AfxMDIFrame120u.AfxFrameOrView120u;
  //afxFrameOrView120u.VScroll.Pos = 379;
  afxFrameOrView120u.Click(980, 230);
  afxFrameOrView120u.Click(348, 237);
  
  var trussStudio;
  trussStudio = Aliases.TrussStudio;
  trussStudio.FormTrussStudio.BCGPDockBarc0000081000510.toolbarMenuBar.ClickItem(9, true);
  trussStudio.wndAfx.Item.ClickItem("Analyze All Trusses", false);
  
  var afxFrameOrView120u;
  afxFrameOrView120u = Aliases.TrussStudio.FormTrussStudio.MDIClient.wndAfx.AfxMDIFrame120u.AfxFrameOrView120u;
  //afxFrameOrView120u.VScroll.Pos = 379;
  afxFrameOrView120u.Click(997, 114);
  
    var TrussStudio;
  TrussStudio = Aliases.TrussStudio;
  TrussStudio.FormTrussStudio.BCGPDockBarc0000081000510.toolbarMenuBar.ClickItem(9, true);
  TrussStudio.wndAfx.Item.ClickItem("Review Trusses", false);

  //FormTS
  var FormTS = Aliases.TrussStudio.FormTS;
    
  UtilitiesForApps.ResizeFormTS();
  
var editProfile = Aliases.TrussStudio.FormTS.Item.wndAfx.AfxMDIFrame120u.page32770.editProfile;
editProfile.SetText("2");
editProfile.Keys("[Tab][Release]");

Aliases.TrussStudio.FormTS.Item.wndAfx.AfxMDIFrame120u.page32770.btnRun.ClickButton();

FormTS.Keys("^s[Release]");
if (Aliases.TrussStudio.dlgComponentSolutionsTrussStudio.Exists)
{
  Aliases.TrussStudio.dlgComponentSolutionsTrussStudio.btnYes.ClickButton();
}

aqUtils.Delay(2000);
FormTS.Keys("^s[Release]");
FormTS.Keys("[Alt]fx[Release]");

  var TrussStudio;
  TrussStudio = Aliases.TrussStudio;
  TrussStudio.FormTrussStudio.BCGPDockBarc0000081000510.toolbarMenuBar.ClickItem(9, true);
  TrussStudio.wndAfx.Item.ClickItem("Analyze All Trusses", false);
  
  if (Aliases.TrussStudio.dlgComponentSolutionsTrussStudio.Exists)
  {
    Aliases.TrussStudio.dlgComponentSolutionsTrussStudio.btnYes.ClickButton();
  }
  
  FormTrussStudio.Keys("^s[Release]");
  
  FormTrussStudio.Keys("[Alt][Down]x");
  
   
}

//********Create Project from DDP************
function CreateProjectAssembly ()
{
var ListviewitemProjectList = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.LvwOutlookMenu.ListviewitemProjectList;
var CreateProjectFromDesignData = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonProjectGroup.CreateProjectFromDesignData;

var CheckBox = Aliases.CSDirector.HwndSource_ImportDesignDataDialog.ImportDesignDataDialog.CheckBox;
var RadbuttonContinue = Aliases.CSDirector.HwndSource_ImportDesignDataDialog.ImportDesignDataDialog.RadbuttonContinue;

var ProjectDescription = Aliases.CSDirector.HwndSource_winCreateProject.winCreateProject.ProjectDescription;
var ProjectMaterialCatalogCombo = Aliases.CSDirector.HwndSource_winCreateProject.winCreateProject.ProjectMaterialCatalogCombo;
var TemplatesComboBox = Aliases.CSDirector.HwndSource_winCreateProject.winCreateProject.TemplatesComboBox;
var CreateProjectButton = Aliases.CSDirector.HwndSource_winCreateProject.winCreateProject.CreateProjectButton;

var RadribbontabTools = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RadribbontabTools
var QuickCheck = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonProcessing.QuickCheck;
var RadtabitemMissingMaterials = Aliases.CSDirector.HwndSource_UnavailableMaterialsDialog.UnavailableMaterialsDialog.WorkspacePane.RadtabitemMissingMaterials;
var UnvailabilityLumberGrid = Aliases.CSDirector.HwndSource_UnavailableMaterialsDialog.UnavailableMaterialsDialog.WorkspacePane.UnvailabilityLumberGrid;

var RadbuttonAutoAddSelectedLumberAndPlatesToP = Aliases.CSDirector.HwndSource_UnavailableMaterialsDialog.UnavailableMaterialsDialog.WorkspacePane.RadbuttonAutoAddSelectedLumberAndPlatesToP;
var RadbuttonClose = Aliases.CSDirector.HwndSource_UnavailableMaterialsDialog.UnavailableMaterialsDialog.WorkspacePane.RadbuttonClose;


//Click Project List
  ListviewitemProjectList.Click();
  CreateProjectFromDesignData.Click();
  Aliases.CSDirector.dlgImportDesignData.cbxFileName.ComboBox.SetText(ProjectSuite.Path + "SettingReportRegressionTest\\Template\\assemply.zip");
  Aliases.CSDirector.dlgImportDesignData.btnOpen.ClickButton();

  CheckBox.ClickButton(true);
  RadbuttonContinue.ClickButton();
  
  ProjectDescription.SetText("Assemply1");
  ProjectMaterialCatalogCombo.Click();
  ProjectMaterialCatalogCombo.Keys("a[Enter]");
  TemplatesComboBox.ClickItem("Studio");
  CreateProjectButton.ClickButton();aqUtils.Delay(3000);
  
  i=0;
  f= true;
  
  while (!Aliases.TrussStudio.dlgComponentSolutionsTrussStudio.Exists)
  {
   i++;
   if (i>3)
   {
    f= false;
    break;
   }
       
  }
  
  if (f)
  {
    Aliases.TrussStudio.dlgComponentSolutionsTrussStudio.btnOK.ClickButton(); 
    Aliases.TrussStudio.dlgUnavailableMaterials.WaitWindow(3000);
  }
  
  if (WaitWin(Aliases.TrussStudio.dlgUnavailableMaterials))
  {
    Aliases.TrussStudio.dlgUnavailableMaterials.btnKeepMaterial.ClickButton();
  
  
    while(Aliases.CSDirector.HwndSource_winCreateProject.Exists)
    {
    }
  
    RadribbontabTools.Click();
    QuickCheck.Click();
    RadtabitemMissingMaterials.Click();
  
    UnvailabilityLumberGrid.Keys("^a[Release]");
    UnvailabilityLumberGrid.ClickCell(0,3,skCtrl)
    UnvailabilityLumberGrid.Keys("5[Enter]");
  
    for (i=0;i<UnvailabilityLumberGrid.wRowCount;i++)
    {
      UnvailabilityLumberGrid.ClickCell(i,4);
    }
    //UnvailabilityLumberGrid.ClickCell(0,4);
    //UnvailabilityLumberGrid.ClickCell(1,4);
    //UnvailabilityLumberGrid.ClickCell(2,4);
    //UnvailabilityLumberGrid.ClickCell(3,4);
  
    RadbuttonAutoAddSelectedLumberAndPlatesToP.ClickButton();
    RadbuttonClose.ClickButton();
  
  }
   
}

function SetPlanElevationsAssemply ()
{
//Plans/Elevations  
var RadtreeviewitemPlansElevations = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.RadTreeMenu.RadtreeviewitemPlansElevations;

var RadtabitemComponents = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.PlansInnerRadTabControl.RadtabitemComponents;
var RadtabitemMiscellaneous = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.PlansInnerRadTabControl.RadtabitemMiscellaneous;
var RadtabitemSets = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.PlansInnerRadTabControl.RadtabitemSets;

var RadTreeViewItem1 = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.PlansInnerRadTabControl.ComponentChildTreeView.RadTreeViewItem.RadTreeViewItem1;
var RadTreeViewItem2 = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.PlansInnerRadTabControl.ComponentChildTreeView.RadTreeViewItem.RadTreeViewItem2;
var RadTreeViewItem3 = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.PlansInnerRadTabControl.ComponentChildTreeView.RadTreeViewItem.RadTreeViewItem3;
var RadTreeViewItem4 = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.PlansInnerRadTabControl.ComponentChildTreeView.RadTreeViewItem.RadTreeViewItem4;
var RadTreeViewItem5 = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.PlansInnerRadTabControl.ComponentChildTreeView.RadTreeViewItem.RadTreeViewItem5;
var RadTreeViewItem6 = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.PlansInnerRadTabControl.ComponentChildTreeView.RadTreeViewItem.RadTreeViewItem6;
var RadTreeViewItem7 = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.PlansInnerRadTabControl.ComponentChildTreeView.RadTreeViewItem.RadTreeViewItem7;
var RadTreeViewItem8 = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.PlansInnerRadTabControl.ComponentChildTreeView.RadTreeViewItem.RadTreeViewItem8;
var RadTreeViewItem9 = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.PlansInnerRadTabControl.ComponentChildTreeView.RadTreeViewItem.RadTreeViewItem9;
var RadTreeViewItem10 = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.PlansInnerRadTabControl.ComponentChildTreeView.RadTreeViewItem.RadTreeViewItem10;
var RadTreeViewItem11 = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.PlansInnerRadTabControl.ComponentChildTreeView.RadTreeViewItem.RadTreeViewItem11;
var RadTreeViewItem12 = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.PlansInnerRadTabControl.ComponentChildTreeView.RadTreeViewItem.RadTreeViewItem12;
var RadTreeViewItem13 = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.PlansInnerRadTabControl.ComponentChildTreeView.RadTreeViewItem.RadTreeViewItem13;
var RadTreeViewItem14 = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.PlansInnerRadTabControl.ComponentChildTreeView.RadTreeViewItem.RadTreeViewItem14;
var RadTreeViewItem15 = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.PlansInnerRadTabControl.ComponentChildTreeView.RadTreeViewItem.RadTreeViewItem15;
var RadTreeViewItem16 = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.PlansInnerRadTabControl.ComponentChildTreeView.RadTreeViewItem.RadTreeViewItem16;
var RadTreeViewItem17 = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.PlansInnerRadTabControl.ComponentChildTreeView.RadTreeViewItem.RadTreeViewItem17;
var RadTreeViewItem18 = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.PlansInnerRadTabControl.ComponentChildTreeView.RadTreeViewItem.RadTreeViewItem18;
var RadTreeViewItem19 = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.PlansInnerRadTabControl.ComponentChildTreeView.RadTreeViewItem.RadTreeViewItem19;

//Plans/Elevations - tab Miscellaneous
  RadtreeviewitemPlansElevations.Click();
  
  RadtabitemComponents.Click();
  
  RadTreeViewItem1.Click(0,0,skCtrl);
  RadTreeViewItem2.Click(0,0,skCtrl);
  RadTreeViewItem3.Click(0,0,skCtrl);
  RadTreeViewItem4.Click(0,0,skCtrl);
  RadTreeViewItem5.Click(0,0,skCtrl);
  RadTreeViewItem6.Click(0,0,skCtrl);
  RadTreeViewItem7.Click(0,0,skCtrl);
  RadTreeViewItem8.Click(0,0,skCtrl);
  RadTreeViewItem9.Click(0,0,skCtrl);
  RadTreeViewItem10.Click(0,0,skCtrl);
  RadTreeViewItem11.Click(0,0,skCtrl);
  RadTreeViewItem12.Click(0,0,skCtrl);
  RadTreeViewItem13.Click(0,0,skCtrl);
  RadTreeViewItem14.Click(0,0,skCtrl);
  RadTreeViewItem15.Click(0,0,skCtrl);
  RadTreeViewItem16.Click(0,0,skCtrl);
  RadTreeViewItem17.Click(0,0,skCtrl);
  RadTreeViewItem18.Click(0,0,skCtrl);
  RadTreeViewItem19.Click(0,0,skCtrl);

//  radPaneGroup = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup;
//  radPaneGroup.PlansInnerRadTabControl.ComponentChildTreeView.RadTreeViewItem2.RadTreeViewItem.Drag(47, 15, -138, -563, skCtrl);
Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.PlansInnerRadTabControl.ComponentChildTreeView.RadTreeViewItem.RadTreeViewItem19.Drag(47, 15, -138, -563, skCtrl);
}

function SetBatchProcessAssemply()
{
 var RadribbontabTools = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RadribbontabTools;
 var BatchProcess = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonProcessing.BatchProcess;
 var ProcessBatchButton = Aliases.CSDirector.HwndSource_winProcessBatch.winProcessBatch.ProcessBatchButton;
 var RadComboBox = Aliases.CSDirector.HwndSource_winProcessBatch.winProcessBatch.RadComboBox;
  
 var dlgProcessBatch = Aliases.CSDirector.dlgProcessBatch;
 var btnOK = Aliases.CSDirector.dlgProcessBatch.btnOK;
 

 RadribbontabTools.Click();
  if (!BatchProcess.Exists)
  {
  RadribbontabTools.Click(0,0);
  }
 BatchProcess.Click();
 RadComboBox.Click();
 RadComboBox.Keys("h[Enter]");
 ProcessBatchButton.Click();
 
 while(!dlgProcessBatch.Exists)
 {
  
 }
 
 btnOK.ClickButton();
 
 
}


function LoginDirector ()
{
  Aliases.CSDirector.HwndSource_winLoginDialog
  var editPassword = Aliases.CSDirector.HwndSource_winLoginDialog.winLoginDialog.Password;
  var editUsername = Aliases.CSDirector.HwndSource_winLoginDialog.winLoginDialog.Username;
  var BtnOk = Aliases.CSDirector.HwndSource_winLoginDialog.winLoginDialog.Okay;
  
  var Fsus = false;
  

    if (WaitWin(Aliases.CSDirector.HwndSource_winLoginDialog))
    {
      editUsername.SetText("admin");
      editPassword.Keys("admin");
      BtnOk.Click();
      Fsus = true ;
    } 

  
  return Fsus;
}

//*******Setup Project training house 1********** 

function SetupProject()
{

  //SetMiscellaneous();
  SetSets();
  //SetPlanElevations();
  SetProjectInfo();
  SetReport();//invoice
  SetDeliveries();
  SetBatchProcess();

}

function SetBatchProcess()
{
 var RadribbontabTools = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RadribbontabTools;
 var BatchProcess = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonProcessing.BatchProcess;
 var ProcessBatchButton = Aliases.CSDirector.HwndSource_winProcessBatch.winProcessBatch.ProcessBatchButton;
 var RadComboBox = Aliases.CSDirector.HwndSource_winProcessBatch.winProcessBatch.RadComboBox;
  
 var dlgProcessBatch = Aliases.CSDirector.dlgProcessBatch;
 var btnOK = Aliases.CSDirector.dlgProcessBatch.btnOK;
 
 RadribbontabTools.Click();
  if (!BatchProcess.Exists)
  {
  RadribbontabTools.Click(0,0);
  }
 
 BatchProcess.Click();
 RadComboBox.Click();
 RadComboBox.Keys("c[Enter]");
 ProcessBatchButton.Click();
 
 while(!dlgProcessBatch.Exists)
 {
  
 }
 
 btnOK.ClickButton();
 
 
}
function SetDeliveries ()
{
  var RadtreeviewitemDeliveries = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.RadTreeMenu.RadtreeviewitemDeliveries;
  var AddLot = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonLotItems.AddLot;
  var CreateLotLotName = Aliases.CSDirector.HwndSource_CreateLotDialog.CreateLotDialog.CreateLotLotName;
  var btnAdd = Aliases.CSDirector.HwndSource_CreateLotDialog.CreateLotDialog.btnAdd;
  
  var RadTreeViewItem = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.LotsTreeViewUI.RadTreeViewItem.RadTreeViewItem;
  
  RadtreeviewitemDeliveries.Click();
  RadTreeViewItem.Click();
  AddLot.Click();
  CreateLotLotName.SetText("AAA");
  btnAdd.ClickButton();
}
function SetReport()
{
  var RadribbontabReports = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RadribbontabReports;
  var InvoicingButton = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonReceivables.InvoicingButton;
  
  var RadbuttonNew = Aliases.CSDirector.HwndSource_vwCSInvoiceForm.vwCSInvoiceForm.RadbuttonNew;
  var RadlistboxitemCsviewmodelsModelsInvoicebas = Aliases.CSDirector.HwndSource_vwCSInvoiceBaseOn.vwCSInvoiceBaseOn.GroupboxItemSelection.RadListBox.RadlistboxitemCsviewmodelsModelsInvoicebas;
  var Okay = Aliases.CSDirector.HwndSource_vwCSInvoiceBaseOn.vwCSInvoiceBaseOn.Okay;
  
  RadribbontabReports.Click();
  if (!InvoicingButton.Exists)
  {
  RadribbontabReports.Click(0,0);
  }
  InvoicingButton.Click();
  RadbuttonNew.ClickButton();
  RadlistboxitemCsviewmodelsModelsInvoicebas.Click();
  Okay.ClickButton();
  
  aqUtils.Delay(2000);
  Aliases.CSDirector.HwndSource_vwCSInvoiceForm.Close();
  
  
}

function SetProjectInfo()
{
//Tasks
var RadtreeviewitemProjectInfo = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.RadTreeMenu.RadtreeviewitemProjectInfo;
var RadtabitemTasks = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.radTabControlProjectInfo.RadtabitemTasks;
var GridViewCell83 = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.radTabControlProjectInfo.GridViewRow.GridViewCell83;
var RadComboBox83 = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.radTabControlProjectInfo.GridViewRow.GridViewCell83.RadComboBox83;
 
//Pricing
var radTabControlProjectInfo = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.radTabControlProjectInfo;
var ProjectPricingTab = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.radTabControlProjectInfo.ProjectPricingTab;
var componentsTotalsGrid = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.radTabControlProjectInfo.componentsTotalsGrid;
var miscellaneousGrid = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.radTabControlProjectInfo.miscellaneousGrid;
var plansGrid = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.radTabControlProjectInfo.plansGrid;

var ComponentsToggleButton = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.radTabControlProjectInfo.ComponentsToggleButton;
var MiscellaneousToggleButton = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.radTabControlProjectInfo.MiscellaneousToggleButton;
var PlansToggleButton = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.radTabControlProjectInfo.PlansToggleButton;


//Tasks
RadtreeviewitemProjectInfo.Click();
RadtabitemTasks.Click();

GridViewCell83.DblClick();
RadComboBox83.ClickItem(1);

//Pricing
ProjectPricingTab.Click();
aqUtils.Delay(10000);

ComponentsToggleButton.ClickButton(true);
MiscellaneousToggleButton.ClickButton(true);
Log.Picture(radTabControlProjectInfo);
ComponentsToggleButton.ClickButton(false);
PlansToggleButton.ClickButton(true);
Log.Picture(radTabControlProjectInfo);

}

function SetPlanElevations ()
{
//Plans/Elevations  
var RadtreeviewitemPlansElevations = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.RadTreeMenu.RadtreeviewitemPlansElevations;

var RadtabitemMiscellaneous = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.PlansInnerRadTabControl.RadtabitemMiscellaneous;
var RadtabitemSets = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.PlansInnerRadTabControl.RadtabitemSets;

var Service = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.PlansInnerRadTabControl.GridViewRow.Service;
var Blocking = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.PlansInnerRadTabControl.GridViewRow2.Blocking;
var Hardware = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.PlansInnerRadTabControl.GridViewRow3.Hardware;

var Item1 = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.PlansInnerRadTabControl.SetsChildTreeView.Item1;

//Plans/Elevations - tab Miscellaneous
  RadtreeviewitemPlansElevations.Click();
  RadtabitemMiscellaneous.Click();
  
  Service.Click(10,10,skCtrl);
  Blocking.Click(10,10,skCtrl);
  Hardware.Click(10,10,skCtrl);
  
  Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.PlansInnerRadTabControl.GridViewRow.Service.Drag(8, 14, -119, -369,skCtrl);
  
  //Plans/Elevations - tab Sets
  RadtabitemSets.Click();
  Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.PlansInnerRadTabControl.SetsChildTreeView.Item1.Drag(56, 16, -183, -306);

}

function SetMiscellaneous ()
{
//Miscellaneous
  var RadtreeviewitemMiscellaneous = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.RadTreeMenu.RadtreeviewitemMiscellaneous;
  var ImportItemsButton = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonMiscItems.ImportItemsButton;
  var radGridView = Aliases.CSDirector.HwndSource_ImportMiscItemDialog.ImportMiscItemDialog.radGridView;
  var CheckboxSelectAll = Aliases.CSDirector.HwndSource_ImportMiscItemDialog.ImportMiscItemDialog.GridViewHeaderCell.CheckboxSelectAll; 
  var RadbuttonOk = Aliases.CSDirector.HwndSource_ImportMiscItemDialog.ImportMiscItemDialog.RadbuttonOk;
  
  //Miscellaneous
  RadtreeviewitemMiscellaneous.Click();
  ImportItemsButton.Click();
  CheckboxSelectAll.ClickButton(true);
  RadbuttonOk.ClickButton();
}

function SetSets()
{
  
  // SETs
  var RadtreeviewitemSets = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.RadTreeMenu.RadtreeviewitemSets;
  
  RadtreeviewitemSets.Click();
  
  var RadTreeViewItem = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.ChildTabControl.ComponentChildTreeView.RadTreeViewItem;
  var T01 = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.ChildTabControl.ComponentChildTreeView.RadTreeViewItem.T01;
  var T02 = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.ChildTabControl.ComponentChildTreeView.RadTreeViewItem.T02;
  var T03 = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.ChildTabControl.ComponentChildTreeView.RadTreeViewItem.T03;
  var T04 = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.ChildTabControl.ComponentChildTreeView.RadTreeViewItem.T04;
    
  //Sets
  T01.Click(0,0,skCtrl);
  T02.Click(0,0,skCtrl);
  T03.Click(0,0,skCtrl);
  T04.Click(0,0,skCtrl);
  Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.ChildTabControl.ComponentChildTreeView.RadTreeViewItem.T02.Drag(46, 12, -180, -423,skCtrl);
}

// ********Create project *******
;
function CreateProject ()
{
var ListviewitemProjectList = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.LvwOutlookMenu.ListviewitemProjectList;
var CreateProjectFromDesignData = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonProjectGroup.CreateProjectFromDesignData;

var CheckBox = Aliases.CSDirector.HwndSource_ImportDesignDataDialog.ImportDesignDataDialog.CheckBox;
var RadbuttonContinue = Aliases.CSDirector.HwndSource_ImportDesignDataDialog.ImportDesignDataDialog.RadbuttonContinue;

var ProjectDescription = Aliases.CSDirector.HwndSource_winCreateProject.winCreateProject.ProjectDescription;
var ProjectMaterialCatalogCombo = Aliases.CSDirector.HwndSource_winCreateProject.winCreateProject.ProjectMaterialCatalogCombo;
var TemplatesComboBox = Aliases.CSDirector.HwndSource_winCreateProject.winCreateProject.TemplatesComboBox;
var CreateProjectButton = Aliases.CSDirector.HwndSource_winCreateProject.winCreateProject.CreateProjectButton;

var RadribbontabTools = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RadribbontabTools
var QuickCheck = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonProcessing.QuickCheck;
var RadtabitemMissingMaterials = Aliases.CSDirector.HwndSource_UnavailableMaterialsDialog.UnavailableMaterialsDialog.WorkspacePane.RadtabitemMissingMaterials;
var UnvailabilityLumberGrid = Aliases.CSDirector.HwndSource_UnavailableMaterialsDialog.UnavailableMaterialsDialog.WorkspacePane.UnvailabilityLumberGrid;

var RadbuttonAutoAddSelectedLumberAndPlatesToP = Aliases.CSDirector.HwndSource_UnavailableMaterialsDialog.UnavailableMaterialsDialog.WorkspacePane.RadbuttonAutoAddSelectedLumberAndPlatesToP;
var RadbuttonClose = Aliases.CSDirector.HwndSource_UnavailableMaterialsDialog.UnavailableMaterialsDialog.WorkspacePane.RadbuttonClose;


//Click Project List
  ListviewitemProjectList.Click();
  CreateProjectFromDesignData.Click();
  Aliases.CSDirector.dlgImportDesignData.cbxFileName.ComboBox.SetText(ProjectSuite.Path + "SettingReportRegressionTest\\Template\\training house1.zip");
  Aliases.CSDirector.dlgImportDesignData.btnOpen.ClickButton();

  CheckBox.ClickButton(true);
  RadbuttonContinue.ClickButton();
  
  ProjectDescription.SetText("Training House 1");
  ProjectMaterialCatalogCombo.Click();
  ProjectMaterialCatalogCombo.Keys("t[Enter]");
  TemplatesComboBox.ClickItem("Studio");
  CreateProjectButton.ClickButton();aqUtils.Delay(3000);
  
//  while (!Aliases.TrussStudio.dlgComponentSolutionsTrussStudio.Exists)
//  {
//    
//  }
//  Aliases.TrussStudio.dlgComponentSolutionsTrussStudio.btnOK.ClickButton(); 
  Aliases.TrussStudio.dlgUnavailableMaterials.WaitWindow(3000);
  
  aqUtils.Delay(5000);
  
  if (WaitWin(Aliases.TrussStudio.dlgUnavailableMaterials))
  {
    Aliases.TrussStudio.dlgUnavailableMaterials.btnKeepMaterial.ClickButton();
  
  
    while(Aliases.CSDirector.HwndSource_winCreateProject.Exists)
    {
    }
  
    RadribbontabTools.Click();
    QuickCheck.Click();
    RadtabitemMissingMaterials.Click();
  
    UnvailabilityLumberGrid.Keys("^a[Release]");
    UnvailabilityLumberGrid.ClickCell(0,3,skCtrl)
    UnvailabilityLumberGrid.Keys("5[Enter]");
    
    for (i=0;i<UnvailabilityLumberGrid.wRowCount;i++)
    {
      UnvailabilityLumberGrid.ClickCell(i,4);
    }
//    UnvailabilityLumberGrid.ClickCell(0,4);
//    UnvailabilityLumberGrid.ClickCell(1,4);
//    UnvailabilityLumberGrid.ClickCell(2,4);
//    UnvailabilityLumberGrid.ClickCell(3,4);
  
    RadbuttonAutoAddSelectedLumberAndPlatesToP.ClickButton();
    RadbuttonClose.ClickButton();
  
  }
   
}

//********Administration********

function SettingAdministration ()
{
  var ListviewitemAdministration = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.LvwOutlookMenu.ListviewitemAdministration;
  
  var OurCompany = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer.NavigatorRadPanelGroup.NavTreeView.Administration.OurCompany;
  var Employees = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer.NavigatorRadPanelGroup.NavTreeView.Administration.Employees;
  var TaxSchemes = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer.NavigatorRadPanelGroup.NavTreeView.ProjectSettings.TaxSchemes;
  var Customers = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer.NavigatorRadPanelGroup.NavTreeView.Administration.Customers;
  var Security = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer.NavigatorRadPanelGroup.NavTreeView.Administration.Security;
  var StartingTemplates = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer.NavigatorRadPanelGroup.NavTreeView.ProjectSettings.StartingTemplates;
  
  ListviewitemAdministration.Click();
  aqUtils.Delay(1000);
  
  //Our Company
  AddValueCompany();
  //Employees
  AddValueEmployees();
  //Tax Schemes
  AddValueTaxSchemes();
  //Customers
  AddValueCustomers();
  //Security
  AddValueSecurity();
  //Starting Templates
  //AddValueStaTemp();
  
  
  
}

function AddValueStaTemp ()
{
  var StartingTemplates = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer.NavigatorRadPanelGroup.NavTreeView.ProjectSettings.StartingTemplates;
  var cboSelectThePresetTemplate = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.cboSelectThePresetTemplate;
  
  StartingTemplates.Click();
  cboSelectThePresetTemplate.ClickItem(2);
  
  
  
}

function AddValueSecurity ()
{
  var Security = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer.NavigatorRadPanelGroup.NavTreeView.Administration.Security;
  //Object Tester-QA01
  var RadTreeViewItem = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.rtv_PermsAndRoles.RtvItemPerson.RadTreeViewItem;
  var rgv_UsersRoles = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.rgv_UsersRoles;
  
  
  Security.Click();
  RadTreeViewItem.Click();
  rgv_UsersRoles.ClickCell(1,1);

}

function AddValueCompany ()
{
 var OurCompany = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer.NavigatorRadPanelGroup.NavTreeView.Administration.OurCompany;
 var RadGridViewCompanies = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.RadGridViewCompanies; 
 
 //General
 var tbCompanyName = Aliases.CSDirector.HwndSource_winOurCompanyInformationDialog.winOurCompanyInformationDialog.RadTabControl.tbCompanyName;
 var cboFacilityType = Aliases.CSDirector.HwndSource_winOurCompanyInformationDialog.winOurCompanyInformationDialog.RadTabControl.GroupboxGeneralInformation.cboFacilityType;
 var tbPhoneNumber = Aliases.CSDirector.HwndSource_winOurCompanyInformationDialog.winOurCompanyInformationDialog.RadTabControl.tbPhoneNumber;
 var tbFaxNumber = Aliases.CSDirector.HwndSource_winOurCompanyInformationDialog.winOurCompanyInformationDialog.RadTabControl.tbFaxNumber;
 var tbEmailAddresses = Aliases.CSDirector.HwndSource_winOurCompanyInformationDialog.winOurCompanyInformationDialog.RadTabControl.tbEmailAddresses;
 var tbEngineeringEmail = Aliases.CSDirector.HwndSource_winOurCompanyInformationDialog.winOurCompanyInformationDialog.RadTabControl.tbEngineeringEmail; 
 var RadbuttonImportNewCompanyLogo = Aliases.CSDirector.HwndSource_winOurCompanyInformationDialog.winOurCompanyInformationDialog.RadTabControl.GroupboxGeneralInformation.RadbuttonImportNewCompanyLogo;
 
 //Addresses - mainling
 var TextboxAddress1 = Aliases.CSDirector.HwndSource_winOurCompanyInformationDialog.winOurCompanyInformationDialog.RadTabControl.GroupboxMailingAddress.TextboxAddress1;
 var TextboxAddress2 = Aliases.CSDirector.HwndSource_winOurCompanyInformationDialog.winOurCompanyInformationDialog.RadTabControl.GroupboxMailingAddress.TextboxAddress2;
 var TextboxCity = Aliases.CSDirector.HwndSource_winOurCompanyInformationDialog.winOurCompanyInformationDialog.RadTabControl.GroupboxMailingAddress.TextboxCity;
 var TextboxCounty = Aliases.CSDirector.HwndSource_winOurCompanyInformationDialog.winOurCompanyInformationDialog.RadTabControl.GroupboxMailingAddress.TextboxCounty;
 var cboStateOrProvince = Aliases.CSDirector.HwndSource_winOurCompanyInformationDialog.winOurCompanyInformationDialog.RadTabControl.GroupboxMailingAddress.cboStateOrProvince;
 var TextboxZipOrPostalCode = Aliases.CSDirector.HwndSource_winOurCompanyInformationDialog.winOurCompanyInformationDialog.RadTabControl.GroupboxMailingAddress.TextboxZipOrPostalCode;
 var cboCountry = Aliases.CSDirector.HwndSource_winOurCompanyInformationDialog.winOurCompanyInformationDialog.RadTabControl.GroupboxMailingAddress.cboCountry;
 //Addresses - shippng
 var TextboxAddress1S = Aliases.CSDirector.HwndSource_winOurCompanyInformationDialog.winOurCompanyInformationDialog.RadTabControl.GroupboxShippingAddress.TextboxAddress1;
 var TextboxAddress2S = Aliases.CSDirector.HwndSource_winOurCompanyInformationDialog.winOurCompanyInformationDialog.RadTabControl.GroupboxShippingAddress.TextboxAddress2;
 var TextboxCityS = Aliases.CSDirector.HwndSource_winOurCompanyInformationDialog.winOurCompanyInformationDialog.RadTabControl.GroupboxShippingAddress.TextboxCity;
 var TextboxCountyS = Aliases.CSDirector.HwndSource_winOurCompanyInformationDialog.winOurCompanyInformationDialog.RadTabControl.GroupboxShippingAddress.TextboxCounty;
 var cboStateOrProvinceS = Aliases.CSDirector.HwndSource_winOurCompanyInformationDialog.winOurCompanyInformationDialog.RadTabControl.GroupboxShippingAddress.cboStateOrProvince;
 var TextboxZipOrPostalCodeS = Aliases.CSDirector.HwndSource_winOurCompanyInformationDialog.winOurCompanyInformationDialog.RadTabControl.GroupboxShippingAddress.TextboxZipOrPostalCode;
 var cboCountryS = Aliases.CSDirector.HwndSource_winOurCompanyInformationDialog.winOurCompanyInformationDialog.RadTabControl.GroupboxShippingAddress.cboCountry;
 
 
 OurCompany.Click();
 
  Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.GridviewcellYourCompany.DblClick(57, 4);

  tbCompanyName.SetText("Your Company 222");tbCompanyName.Keys("[Tab][Release]");
  cboFacilityType.ClickItem(1);
  tbPhoneNumber.SetText("222");tbPhoneNumber.Keys("[Tab][Release]");
  tbFaxNumber.SetText("222");tbFaxNumber.Keys("[Tab][Release]");
  tbEmailAddresses.SetText("C222@hotmail.com");tbEmailAddresses.Keys("[Tab][Release]");
  tbEngineeringEmail.SetText("EngC222@hotmail.com");tbEngineeringEmail.Keys("[Tab][Release]");
  
  RadbuttonImportNewCompanyLogo.ClickButton();
  Aliases.CSDirector.dlg.cbxFileName.ComboBox.SetText(ProjectSuite.Path + "SettingReportRegressionTest\\Template\\LOGO\\OurCompany Logo.jpg");
  Aliases.CSDirector.dlg.btnOpen.ClickButton();
  
  Aliases.CSDirector.HwndSource_winOurCompanyInformationDialog.winOurCompanyInformationDialog.RadTabControl.RadtabitemAddresses.Click(20, 11);
  
  TextboxAddress1.SetText("222A");TextboxAddress1.Keys("[Tab][Release]");
  TextboxAddress2.SetText("222B");TextboxAddress2.Keys("[Tab][Release]");
  TextboxCity.SetText("222C");TextboxCity.Keys("[Tab][Release]");
  TextboxCounty.SetText("222D");TextboxCounty.Keys("[Tab][Release]");
  cboStateOrProvince.Keys("Alabama");
  TextboxZipOrPostalCode.SetText("222E");TextboxZipOrPostalCode.Keys("[Tab][Release]");
  cboCountry.Keys("United States");
  
  TextboxAddress1S.SetText("222A");TextboxAddress1S.Keys("[Tab][Release]");
  TextboxAddress2S.SetText("222B");TextboxAddress2S.Keys("[Tab][Release]");
  TextboxCityS.SetText("222C");TextboxCityS.Keys("[Tab][Release]");
  TextboxCountyS.SetText("222D");TextboxCountyS.Keys("[Tab][Release]");
  cboStateOrProvinceS.Keys("Alabama");
  TextboxZipOrPostalCodeS.SetText("222E");TextboxZipOrPostalCodeS.Keys("[Tab][Release]");
  cboCountryS.Keys("United States");
  
  Aliases.CSDirector.HwndSource_winOurCompanyInformationDialog.winOurCompanyInformationDialog.AddCustomerButton.ClickButton();
  
} 


function AddValueTaxSchemes ()
{
  var TaxSchemes = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer.NavigatorRadPanelGroup.NavTreeView.ProjectSettings.TaxSchemes;
  var AddAdminItemButton = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonAdminItems.AddAdminItemButton;
  
  var TaxSchemeName = Aliases.CSDirector.HwndSource_TaxSchemeDialog.TaxSchemeDialog.TaxSchemeName;
  var tbTax1 = Aliases.CSDirector.HwndSource_TaxSchemeDialog.TaxSchemeDialog.GroupboxCustomTaxScheme.tbTax1;
  var tbRate1 = Aliases.CSDirector.HwndSource_TaxSchemeDialog.TaxSchemeDialog.GroupboxCustomTaxScheme.tbRate1;
  var tbTaxId1 = Aliases.CSDirector.HwndSource_TaxSchemeDialog.TaxSchemeDialog.GroupboxCustomTaxScheme.tbTaxId1;
  var tbTax2 = Aliases.CSDirector.HwndSource_TaxSchemeDialog.TaxSchemeDialog.GroupboxCustomTaxScheme.tbTax2;
  var tbRate2 = Aliases.CSDirector.HwndSource_TaxSchemeDialog.TaxSchemeDialog.GroupboxCustomTaxScheme.tbRate2;
  var tbTaxId2 = Aliases.CSDirector.HwndSource_TaxSchemeDialog.TaxSchemeDialog.GroupboxCustomTaxScheme.tbTaxId2;
  
  var FinishButton = Aliases.CSDirector.HwndSource_TaxSchemeDialog.TaxSchemeDialog.FinishButton;
  
  TaxSchemes.Click();
  AddAdminItemButton.Click();
  TaxSchemeName.Keys("Tax Scheme 1");
  tbTax1.Keys("TaxScheme_1_1");
  tbRate1.Keys("10");
  tbTaxId1.Keys("1_1");
  tbTax2.Keys("TaxScheme_1_2");
  tbRate2.Keys("20");
  tbTaxId2.Keys("1_2");
  
  FinishButton.ClickButton();
}

function AddValueEmployees ()
{
  var Employees = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer.NavigatorRadPanelGroup.NavTreeView.Administration.Employees;
  var AddAdminItemButton = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonAdminItems.AddAdminItemButton;
  
  var RadtabitemBusiness = Aliases.CSDirector.HwndSource_winPersonInformationDialog.winPersonInformationDialog.RadTabControl.RadtabitemBusiness;
  var tbFirstName = Aliases.CSDirector.HwndSource_winPersonInformationDialog.winPersonInformationDialog.RadTabControl.tbFirstName;
  var tbLastName = Aliases.CSDirector.HwndSource_winPersonInformationDialog.winPersonInformationDialog.RadTabControl.tbLastName;
  var tbPersonId = Aliases.CSDirector.HwndSource_winPersonInformationDialog.winPersonInformationDialog.RadTabControl.tbPersonId;
  var tbBusinessPhoneNumber = Aliases.CSDirector.HwndSource_winPersonInformationDialog.winPersonInformationDialog.RadTabControl.tbBusinessPhoneNumber;
  var tbBusinessCellNumber = Aliases.CSDirector.HwndSource_winPersonInformationDialog.winPersonInformationDialog.RadTabControl.tbBusinessCellNumber;
  var tbEmailAddresses = Aliases.CSDirector.HwndSource_winPersonInformationDialog.winPersonInformationDialog.RadTabControl.tbEmailAddresses;
  
  var RadtabitemPersonal = Aliases.CSDirector.HwndSource_winPersonInformationDialog.winPersonInformationDialog.RadTabControl.RadtabitemPersonal;
  var TextboxAddress1 = Aliases.CSDirector.HwndSource_winPersonInformationDialog.winPersonInformationDialog.RadTabControl.GroupboxAddress.TextboxAddress1;
  var TextboxAddress2 = Aliases.CSDirector.HwndSource_winPersonInformationDialog.winPersonInformationDialog.RadTabControl.GroupboxAddress.TextboxAddress2;
  var TextboxCity = Aliases.CSDirector.HwndSource_winPersonInformationDialog.winPersonInformationDialog.RadTabControl.GroupboxAddress.TextboxCity;
  var TextboxCounty = Aliases.CSDirector.HwndSource_winPersonInformationDialog.winPersonInformationDialog.RadTabControl.GroupboxAddress.TextboxCounty;
  var TextboxStateOrProvince = Aliases.CSDirector.HwndSource_winPersonInformationDialog.winPersonInformationDialog.RadTabControl.GroupboxAddress.TextboxStateOrProvince;
  var TextboxZipOrPostalCode = Aliases.CSDirector.HwndSource_winPersonInformationDialog.winPersonInformationDialog.RadTabControl.GroupboxAddress.TextboxZipOrPostalCode;
  var TextboxEmergencyContact = Aliases.CSDirector.HwndSource_winPersonInformationDialog.winPersonInformationDialog.RadTabControl.GroupboxPhone.TextboxEmergencyContact;
  var TextboxEmergencyPhone = Aliases.CSDirector.HwndSource_winPersonInformationDialog.winPersonInformationDialog.RadTabControl.GroupboxPhone.TextboxEmergencyPhone;
  var TextboxHomePhone = Aliases.CSDirector.HwndSource_winPersonInformationDialog.winPersonInformationDialog.RadTabControl.GroupboxPhone.TextboxHomePhone;
  var TextboxCellPhone = Aliases.CSDirector.HwndSource_winPersonInformationDialog.winPersonInformationDialog.RadTabControl.GroupboxPhone.TextboxCellPhone;
  
  var RadtabitemAdmin = Aliases.CSDirector.HwndSource_winPersonInformationDialog.winPersonInformationDialog.RadTabControl.RadtabitemAdmin;
  var TextboxUsername = Aliases.CSDirector.HwndSource_winPersonInformationDialog.winPersonInformationDialog.RadTabControl.TextboxUsername;
  
  var SavePersonButton = Aliases.CSDirector.HwndSource_winPersonInformationDialog.winPersonInformationDialog.SavePersonButton;
  
  
  Employees.Click();
  AddAdminItemButton.Click();
  
  //Tab Business
  RadtabitemBusiness.Click();
  tbFirstName.SetText("VNQA");tbFirstName.Keys("[Tab][Release]");
  tbLastName.SetText("01");tbLastName.Keys("[Tab][Release]");
  tbPersonId.SetText("VNQA-01");tbPersonId.Keys("[Tab][Release]");
  tbBusinessPhoneNumber.SetText("01");tbBusinessPhoneNumber.Keys("[Tab][Release]");
  tbBusinessCellNumber.SetText("01");tbBusinessCellNumber.Keys("[Tab][Release]");
  tbEmailAddresses.SetText("VNQA-01@hotmail.com");tbEmailAddresses.Keys("[Tab][Release]");
  
  //Tab Personal
  RadtabitemPersonal.Click();
  TextboxAddress1.SetText("01A");TextboxAddress1.Keys("[Tab][Release]");
  TextboxAddress2.SetText("01B");TextboxAddress2.Keys("[Tab][Release]");
  TextboxCity.SetText("01C");TextboxCity.Keys("[Tab][Release]");
  TextboxCounty.SetText("01E");TextboxCounty.Keys("[Tab][Release]");
  TextboxStateOrProvince.SetText("01D");TextboxStateOrProvince.Keys("[Tab][Release]");
  TextboxZipOrPostalCode.SetText("01F");TextboxZipOrPostalCode.Keys("[Tab][Release]");
  TextboxEmergencyContact.SetText("01");TextboxEmergencyContact.Keys("[Tab][Release]");
  TextboxEmergencyPhone.SetText("01");TextboxEmergencyPhone.Keys("[Tab][Release]");
  TextboxHomePhone.SetText("01");TextboxHomePhone.Keys("[Tab][Release]");
  TextboxCellPhone.SetText("01");TextboxCellPhone.Keys("[Tab][Release]");
  
  //Tab Admin
  RadtabitemAdmin.Click();
  TextboxUsername.SetText("VNQA01");TextboxUsername.Keys("[Tab][Release]");
  
  SavePersonButton.ClickButton();
  
  
  
}

function AddValueCustomers()
{
 var Customers = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer.NavigatorRadPanelGroup.NavTreeView.Administration.Customers;
 var AddAdminItemButton = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonAdminItems.AddAdminItemButton;
 
 var RadtabitemGeneral = Aliases.CSDirector.HwndSource_winCustomerInformationDialog.winCustomerInformationDialog.RadTabControl.RadtabitemGeneral;
 var tbCompanyName = Aliases.CSDirector.HwndSource_winCustomerInformationDialog.winCustomerInformationDialog.RadTabControl.tbCompanyName;
 var cboSalesPerson = Aliases.CSDirector.HwndSource_winCustomerInformationDialog.winCustomerInformationDialog.RadTabControl.cboSalesPerson;
 var cboDefaultRole = Aliases.CSDirector.HwndSource_winCustomerInformationDialog.winCustomerInformationDialog.RadTabControl.cboDefaultRole;
 var PhoneNumber =Aliases.CSDirector.HwndSource_winCustomerInformationDialog.winCustomerInformationDialog.RadTabControl.PhoneNumber;
 var EmailAddresses = Aliases.CSDirector.HwndSource_winCustomerInformationDialog.winCustomerInformationDialog.RadTabControl.EmailAddresses;
 var FaxNumber = Aliases.CSDirector.HwndSource_winCustomerInformationDialog.winCustomerInformationDialog.RadTabControl.FaxNumber;
 var AccountNumber = Aliases.CSDirector.HwndSource_winCustomerInformationDialog.winCustomerInformationDialog.RadTabControl.AccountNumber;
 var TaxNumber = Aliases.CSDirector.HwndSource_winCustomerInformationDialog.winCustomerInformationDialog.RadTabControl.TaxNumber;
 var ResaleCertificate = Aliases.CSDirector.HwndSource_winCustomerInformationDialog.winCustomerInformationDialog.RadTabControl.ResaleCertificate;
 
 var RadtabitemAddresses = Aliases.CSDirector.HwndSource_winCustomerInformationDialog.winCustomerInformationDialog.RadTabControl.RadtabitemAddresses;
 var TextboxAddress1 = Aliases.CSDirector.HwndSource_winCustomerInformationDialog.winCustomerInformationDialog.RadTabControl.GroupboxMailingAddress.TextboxAddress1;
 var TextboxAddress2 = Aliases.CSDirector.HwndSource_winCustomerInformationDialog.winCustomerInformationDialog.RadTabControl.GroupboxMailingAddress.TextboxAddress2;
 var TextboxCity = Aliases.CSDirector.HwndSource_winCustomerInformationDialog.winCustomerInformationDialog.RadTabControl.GroupboxMailingAddress.TextboxCity;
 var TextboxCounty = Aliases.CSDirector.HwndSource_winCustomerInformationDialog.winCustomerInformationDialog.RadTabControl.GroupboxMailingAddress.TextboxCounty;
 var cboStateOrProvince = Aliases.CSDirector.HwndSource_winCustomerInformationDialog.winCustomerInformationDialog.RadTabControl.GroupboxMailingAddress.cboStateOrProvince;
 var TextboxZipOrPostalCode = Aliases.CSDirector.HwndSource_winCustomerInformationDialog.winCustomerInformationDialog.RadTabControl.GroupboxMailingAddress.TextboxZipOrPostalCode;
 var cboCountry = Aliases.CSDirector.HwndSource_winCustomerInformationDialog.winCustomerInformationDialog.RadTabControl.GroupboxMailingAddress.cboCountry;
 
 var TextboxAddress1S =  Aliases.CSDirector.HwndSource_winCustomerInformationDialog.winCustomerInformationDialog.RadTabControl.GroupboxShippingAddress.TextboxAddress1;
 var TextboxAddress2S = Aliases.CSDirector.HwndSource_winCustomerInformationDialog.winCustomerInformationDialog.RadTabControl.GroupboxShippingAddress.TextboxAddress2;
 var TextboxCityS = Aliases.CSDirector.HwndSource_winCustomerInformationDialog.winCustomerInformationDialog.RadTabControl.GroupboxShippingAddress.TextboxCity;
 var TextboxCountyS = Aliases.CSDirector.HwndSource_winCustomerInformationDialog.winCustomerInformationDialog.RadTabControl.GroupboxShippingAddress.TextboxCounty;
 var cboStateOrProvinceS = Aliases.CSDirector.HwndSource_winCustomerInformationDialog.winCustomerInformationDialog.RadTabControl.GroupboxShippingAddress.cboStateOrProvince;
 var TextboxZipOrPostalCodeS = Aliases.CSDirector.HwndSource_winCustomerInformationDialog.winCustomerInformationDialog.RadTabControl.GroupboxShippingAddress.TextboxZipOrPostalCode;
 var cboCountryS = Aliases.CSDirector.HwndSource_winCustomerInformationDialog.winCustomerInformationDialog.RadTabControl.GroupboxShippingAddress.cboCountry;
 
 var TextboxAddress1I = Aliases.CSDirector.HwndSource_winCustomerInformationDialog.winCustomerInformationDialog.RadTabControl.GroupboxInvoicingAddress.TextboxAddress1;
 var TextboxAddress2I = Aliases.CSDirector.HwndSource_winCustomerInformationDialog.winCustomerInformationDialog.RadTabControl.GroupboxInvoicingAddress.TextboxAddress2;
 var TextboxCityI = Aliases.CSDirector.HwndSource_winCustomerInformationDialog.winCustomerInformationDialog.RadTabControl.GroupboxInvoicingAddress.TextboxCity;
 var TextboxCountyI = Aliases.CSDirector.HwndSource_winCustomerInformationDialog.winCustomerInformationDialog.RadTabControl.GroupboxInvoicingAddress.TextboxCounty;
 var cboStateOrProvinceI = Aliases.CSDirector.HwndSource_winCustomerInformationDialog.winCustomerInformationDialog.RadTabControl.GroupboxInvoicingAddress.cboStateOrProvince;
 var TextboxZipOrPostalCodeI = Aliases.CSDirector.HwndSource_winCustomerInformationDialog.winCustomerInformationDialog.RadTabControl.GroupboxInvoicingAddress.TextboxZipOrPostalCode;
 var cboCountryI = Aliases.CSDirector.HwndSource_winCustomerInformationDialog.winCustomerInformationDialog.RadTabControl.GroupboxInvoicingAddress.cboCountry;
 
 var RadbuttonSave = Aliases.CSDirector.HwndSource_winCustomerInformationDialog.winCustomerInformationDialog.RadbuttonSave;
 
  //contacs
  var RadtabitemContacts = Aliases.CSDirector.HwndSource_winCustomerInformationDialog.winCustomerInformationDialog.RadTabControl.RadtabitemContacts;
 var CSGridView = Aliases.CSDirector.HwndSource_winCustomerInformationDialog.winCustomerInformationDialog.RadTabControl.CSGridView;
 
 
 Customers.Click();
 AddAdminItemButton.Click();
 
 RadtabitemGeneral.Click();
 tbCompanyName.SetText("Cus100");tbCompanyName.Keys("[Tab][Release]");
 cboSalesPerson.ClickItem(1);
 cboDefaultRole.ClickItem(1);
 PhoneNumber.SetText("100");PhoneNumber.Keys("[Tab][Release]");
 EmailAddresses.SetText("100@hotmail.com");EmailAddresses.Keys("[Tab][Release]");
 FaxNumber.SetText("100");FaxNumber.Keys("[Tab][Release]");
 //AccountNumber.SetText("100");
 //TaxNumber.SetText("100");
 //ResaleCertificate.SetText("100");
 
 RadtabitemAddresses.Click();
  
  TextboxAddress1.SetText("100A");TextboxAddress1.Keys("[Tab][Release]");
  TextboxAddress2.SetText("100B");TextboxAddress2.Keys("[Tab][Release]");
  TextboxCity.SetText("100C");TextboxCity.Keys("[Tab][Release]");
  TextboxCounty.SetText("100D");TextboxCounty.Keys("[Tab][Release]");
  cboStateOrProvince.Keys("Alabama");
  TextboxZipOrPostalCode.SetText("100E");TextboxZipOrPostalCode.Keys("[Tab][Release]");
  cboCountry.Keys("United States");

  TextboxAddress1S.SetText("100A");TextboxAddress1S.Keys("[Tab][Release]");
  TextboxAddress2S.SetText("100B");TextboxAddress2S.Keys("[Tab][Release]");
  TextboxCityS.SetText("100C");TextboxCityS.Keys("[Tab][Release]");
  TextboxCountyS.SetText("100D");TextboxCountyS.Keys("[Tab][Release]");
  cboStateOrProvinceS.Keys("Alabama");
  TextboxZipOrPostalCodeS.SetText("100E");TextboxZipOrPostalCodeS.Keys("[Tab][Release]");
  cboCountryS.Keys("United States");
  
  TextboxAddress1I.SetText("100A");TextboxAddress1I.Keys("[Tab][Release]");
  TextboxAddress2I.SetText("100B");TextboxAddress2I.Keys("[Tab][Release]");
  TextboxCityI.SetText("100C");TextboxCityI.Keys("[Tab][Release]");
  TextboxCountyI.SetText("100D");TextboxCountyI.Keys("[Tab][Release]");
  cboStateOrProvinceI.Keys("Alabama");
  TextboxZipOrPostalCodeI.SetText("100E");TextboxZipOrPostalCodeI.Keys("[Tab][Release]");
  cboCountryI.Keys("United States");
  
  RadtabitemContacts.Click();
  Aliases.CSDirector.HwndSource_winCustomerInformationDialog.winCustomerInformationDialog.RadTabControl.CSGridView.Click(149, 74);
  CSGridView.Keys("[Tab][Tab]100");
  Aliases.CSDirector.HwndSource_winCustomerInformationDialog.winCustomerInformationDialog.RadTabControl.CSGridView.Click(149, 74);
  CSGridView.Keys(" [Tab][Tab]100");
 
 RadbuttonSave.ClickButton();
  
}


//*****MaterialCatalog********
function SettingMaterialCatalog ()
{
  var MaterialCatalogs = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonSettingsItems.MaterialCatalogs;
  var HwndSource_MaterialCatalogDialog = Aliases.CSDirector.HwndSource_MaterialCatalogDialog;
  
  var CreateMaterialCatalogItemButton = Aliases.CSDirector.HwndSource_MaterialCatalogDialog.MaterialCatalogDialog.RibbonMaterialInventoryItems1.CreateMaterialCatalogItemButton;
  var NewMTName = Aliases.CSDirector.HwndSource_MaterialCatalogEditDialog.MaterialCatalogEditDialog.NewMTName;
  var ActionBt = Aliases.CSDirector.HwndSource_MaterialCatalogEditDialog.MaterialCatalogEditDialog.ActionBt;
  
  var btnAddMaterial = Aliases.CSDirector.HwndSource_MaterialCatalogDialog.MaterialCatalogDialog.RibbonMaterialInventoryItems.btnAddMaterial;
  var btnDeleteItem = Aliases.CSDirector.HwndSource_MaterialCatalogDialog.MaterialCatalogDialog.RibbonMaterialInventoryItems.btnDeleteItem;
  
  //Main
  MaterialCatalogs.Click();
  HwndSource_MaterialCatalogDialog.WaitWindow(2000);
/*  
//******Caatalog 1**********************
  CreateMaterialCatalogItemButton.Click();
  NewMTName.SetText("Training House");
  ActionBt.ClickButton();
  
  //----Note : 1,2,3 = catalog 1,2,3
  //Lumber
  AddValueLumber(1);
  //Plates
  AddValuePlates(1);
  //Miscellaneous
  AddValueMiscellaneous(1);
*/  
//******Caatalog 2**********************
  CreateMaterialCatalogItemButton.Click();
  NewMTName.SetText("Smoke Test");
  ActionBt.ClickButton();
  
  //Lumber
  AddValueLumber(2);
  //Plates
  AddValuePlates(2);

/*  
//******Caatalog 3**********************
  CreateMaterialCatalogItemButton.Click();
  NewMTName.SetText("Assembly");
  ActionBt.ClickButton();
  
  //Lumber
  AddValueLumber(3);
  //Plates
  AddValuePlates(3);
  //Miscellaneous
  AddValueMiscellaneous(3);
  
*/
HwndSource_MaterialCatalogDialog.Close();
   
}

function AddValueMiscellaneous(arg)
{

 
  //Miscellaneous
  var SelectionPricing = Aliases.CSDirector.HwndSource_MaterialCatalogDialog.MaterialCatalogDialog.radDocking.RadSplitContainer.NavigatorRadPanelGroup.MaterialNavTreeView.Miscellaneous.UserDefined.SelectionPricing;
  
  var RadGridViewMiscellaneous = Aliases.CSDirector.HwndSource_MaterialCatalogDialog.MaterialCatalogDialog.radDocking.RadSplitContainer1.WorkspacePaneGroup.RadGridViewMiscellaneous;
  var btnAddMaterial = Aliases.CSDirector.HwndSource_MaterialCatalogDialog.MaterialCatalogDialog.RibbonMaterialInventoryItems.btnAddMaterial;
  var btnDeleteItem = Aliases.CSDirector.HwndSource_MaterialCatalogDialog.MaterialCatalogDialog.RibbonMaterialInventoryItems.btnDeleteItem;
  
  var cboCategoty = Aliases.CSDirector.HwndSource_MaterialCatalogDialog.MaterialCatalogDialog.radDocking.RadSplitContainer1.WorkspacePaneGroup.cboCategoty;
  var tbItem = Aliases.CSDirector.HwndSource_MaterialCatalogDialog.MaterialCatalogDialog.radDocking.RadSplitContainer1.WorkspacePaneGroup.tbItem;
  
 if (arg==1)
 { 
  
  SelectionPricing.Click();
  
  while (RadGridViewMiscellaneous.wRowCount!=0)
  {
    RadGridViewMiscellaneous.ClickCell(0,0);
    btnDeleteItem.Click();
    Aliases.CSDirector.HwndSource_PromptOkCancelView.PromptOkCancelView.btnFinish.ClickButton();
  } 
  
  btnAddMaterial.Click();
  RadGridViewMiscellaneous.ClickCell(0,0);
  cboCategoty.Keys("Service[Tab]");
  tbItem.Keys("Crane[Tab]");
  cboCategoty.Keys("h[Tab]");
  tbItem.Keys("250[Tab]");
  tbItem.Keys("250[Tab]");
  tbItem.Keys("0[Tab]");
  
  btnAddMaterial.Click();
  RadGridViewMiscellaneous.ClickCell(1,0);
  cboCategoty.Keys("Hardware[Tab]");
  tbItem.Keys("HD1[Tab]");
  cboCategoty.Keys("e[Tab]");
  tbItem.Keys("15[Tab]");
  tbItem.Keys("15[Tab]");
  tbItem.Keys("0[Tab]");
  RadGridViewMiscellaneous.ClickCell(1,6);;
  
  btnAddMaterial.Click();
  RadGridViewMiscellaneous.ClickCell(2,0);
  cboCategoty.Keys("Blocking[Tab]");
  tbItem.Keys("Block1[Tab]");
  cboCategoty.Keys("e[Tab]");
  tbItem.Keys("25[Tab]");
  tbItem.Keys("25[Tab]");
  tbItem.Keys("0[Tab]");
 } 
 if (arg==3)
 { 
  SelectionPricing.Click();
  
  while (RadGridViewMiscellaneous.wRowCount!=0)
  {
    RadGridViewMiscellaneous.ClickCell(0,0);
    btnDeleteItem.Click();
    Aliases.CSDirector.HwndSource_PromptOkCancelView.PromptOkCancelView.btnFinish.ClickButton();
  } 
  
  btnAddMaterial.Click();
  RadGridViewMiscellaneous.ClickCell(0,0);
  cboCategoty.Keys("Service[Tab]");
  tbItem.Keys("Service01[Tab]");
  cboCategoty.Keys("m[Tab]");
  tbItem.Keys("3[Tab]");
  tbItem.Keys("4[Tab]");
  tbItem.Keys("[Tab]");
  
  btnAddMaterial.Click();
  RadGridViewMiscellaneous.ClickCell(1,0);
  cboCategoty.Keys("Pre-Manufactured Shear Panel01[Tab]");
  tbItem.Keys("Pre-Manufactured Shear Panel[Tab]");
  cboCategoty.Keys("b[Down][Tab]");
  tbItem.Keys("3[Tab]");
  tbItem.Keys("4[Tab]");
  tbItem.Keys("[Tab]");
  
  btnAddMaterial.Click();
  RadGridViewMiscellaneous.ClickCell(2,0);
  cboCategoty.Keys("Hold Down[Tab]");
  tbItem.Keys("HoldDown01[Tab]");
  cboCategoty.Keys("b[Tab]");
  tbItem.Keys("3[Tab]");
  tbItem.Keys("4[Tab]");
  tbItem.Keys("[Tab]");
  
  btnAddMaterial.Click();
  RadGridViewMiscellaneous.ClickCell(3,0);
  cboCategoty.Keys("Hardware[Tab]");
  tbItem.Keys("Hardware01[Tab]");
  cboCategoty.Keys("s[Tab]");
  tbItem.Keys("3[Tab]");
  tbItem.Keys("4[Tab]");
  tbItem.Keys("[Tab]");
  
  btnAddMaterial.Click();
  RadGridViewMiscellaneous.ClickCell(4,0);
  cboCategoty.Keys("Blocking[Tab]");
  tbItem.Keys("Blocking01[Tab]");
  cboCategoty.Keys("e[Tab]");
  tbItem.Keys("3[Tab]");
  tbItem.Keys("4[Tab]");
  tbItem.Keys("[Tab]");
  
  //****SelectHangers
  var SelectHangers = Aliases.CSDirector.HwndSource_MaterialCatalogDialog.MaterialCatalogDialog.RibbonMaterialPickLength.SelectHangers;
  var TextBox = Aliases.CSDirector.HwndSource_HangerSelectorView.HangerSelectorView.TextBox;
  var Textblock = Aliases.CSDirector.HwndSource_HangerSelectorView.HangerSelectorView.HangerList.Textblock;
  var RadbuttonOk = Aliases.CSDirector.HwndSource_HangerSelectorView.HangerSelectorView.RadbuttonOk;
  
  SelectHangers.Click();
  TextBox.SetText("B1.56/9.25");
  Textblock.Click();
  RadbuttonOk.ClickButton();
  RadGridViewMiscellaneous.DblClickCell(5,3);
  tbItem.Keys("3[Tab]");
  tbItem.Keys("4[Tab]");
  tbItem.Keys("[Tab]");
  
  SelectHangers.Click();
  TextBox.SetText("B1.56/11.88");
  Textblock.Click();
  RadbuttonOk.ClickButton();
  RadGridViewMiscellaneous.DblClickCell(6,3);
  tbItem.Keys("3[Tab]");
  tbItem.Keys("4[Tab]");
  tbItem.Keys("[Tab]");
  
  SelectHangers.Click();
  TextBox.SetText("B1.81/11.88");
  Textblock.Click();
  RadbuttonOk.ClickButton();
  RadGridViewMiscellaneous.DblClickCell(7,3);
  tbItem.Keys("3[Tab]");
  tbItem.Keys("4[Tab]");
  tbItem.Keys("[Tab]");
  
  SelectHangers.Click();
  TextBox.SetText("B1.81/16");
  Textblock.Click();
  RadbuttonOk.ClickButton();
  RadGridViewMiscellaneous.DblClickCell(8,3);
  tbItem.Keys("3[Tab]");
  tbItem.Keys("4[Tab]");
  tbItem.Keys("[Tab]");
  
  SelectHangers.Click();
  TextBox.SetText("B1.81/9.5");
  Textblock.Click();
  RadbuttonOk.ClickButton();
  RadGridViewMiscellaneous.DblClickCell(9,3);
  tbItem.Keys("3[Tab]");
  tbItem.Keys("4[Tab]");
  tbItem.Keys("[Tab]");
  
  SelectHangers.Click();
  TextBox.SetText("B1.81/14");
  Textblock.Click();
  RadbuttonOk.ClickButton();
  RadGridViewMiscellaneous.DblClickCell(10,3);
  tbItem.Keys("3[Tab]");
  tbItem.Keys("4[Tab]");
  tbItem.Keys("[Tab]");
  
  SelectHangers.Click();
  TextBox.SetText("B1.56/11.25");
  Textblock.Click();
  RadbuttonOk.ClickButton();
  RadGridViewMiscellaneous.DblClickCell(11,3);
  tbItem.Keys("3[Tab]");
  tbItem.Keys("4[Tab]");
  tbItem.Keys("[Tab]");
  
 }
  
} 

function addSelectHanger()
{
  //Miscellaneous
  var SelectionPricing = Aliases.CSDirector.HwndSource_MaterialCatalogDialog.MaterialCatalogDialog.radDocking.RadSplitContainer.NavigatorRadPanelGroup.MaterialNavTreeView.Miscellaneous.UserDefined.SelectionPricing;
  
  var RadGridViewMiscellaneous = Aliases.CSDirector.HwndSource_MaterialCatalogDialog.MaterialCatalogDialog.radDocking.RadSplitContainer1.WorkspacePaneGroup.RadGridViewMiscellaneous;
  var btnAddMaterial = Aliases.CSDirector.HwndSource_MaterialCatalogDialog.MaterialCatalogDialog.RibbonMaterialInventoryItems.btnAddMaterial;
  var btnDeleteItem = Aliases.CSDirector.HwndSource_MaterialCatalogDialog.MaterialCatalogDialog.RibbonMaterialInventoryItems.btnDeleteItem;
  
  var cboCategoty = Aliases.CSDirector.HwndSource_MaterialCatalogDialog.MaterialCatalogDialog.radDocking.RadSplitContainer1.WorkspacePaneGroup.cboCategoty;
  var tbItem = Aliases.CSDirector.HwndSource_MaterialCatalogDialog.MaterialCatalogDialog.radDocking.RadSplitContainer1.WorkspacePaneGroup.tbItem;
  var SelectHangers = Aliases.CSDirector.HwndSource_MaterialCatalogDialog.MaterialCatalogDialog.RibbonMaterialPickLength.SelectHangers;
  var TextBox = Aliases.CSDirector.HwndSource_HangerSelectorView.HangerSelectorView.TextBox;
  var Textblock = Aliases.CSDirector.HwndSource_HangerSelectorView.HangerSelectorView.HangerList.Textblock;
  var RadbuttonOk = Aliases.CSDirector.HwndSource_HangerSelectorView.HangerSelectorView.RadbuttonOk;
  
  SelectHangers.Click();
  TextBox.SetText("B1.56/9.25");
  Textblock.Click();
  RadbuttonOk.ClickButton();
  RadGridViewMiscellaneous.DblClickCell(5,3);
  tbItem.Keys("3[Tab]");
  tbItem.Keys("4[Tab]");
  tbItem.Keys("[Tab]");
  
  SelectHangers.Click();
  TextBox.SetText("B1.56/11.88");
  Textblock.Click();
  RadbuttonOk.ClickButton();
  RadGridViewMiscellaneous.DblClickCell(6,3);
  tbItem.Keys("3[Tab]");
  tbItem.Keys("4[Tab]");
  tbItem.Keys("[Tab]");
  
}

function AddValuePlates(arg)
{
  var btnAddMaterial = Aliases.CSDirector.HwndSource_MaterialCatalogDialog.MaterialCatalogDialog.RibbonMaterialInventoryItems.btnAddMaterial;
  //add product 
  var HwndSource_winProdPriority = Aliases.CS32BitProxyHost.HwndSource_winProdPriority;
  var btnAddProduct = Aliases.CS32BitProxyHost.HwndSource_winProdPriority.winProdPriority.btnAddProduct;

  var ListType = Aliases.CS32BitProxyHost.HwndSource_winBuildProducts.winBuildProducts.ListSpecies;
  var ListGauge = Aliases.CS32BitProxyHost.HwndSource_winBuildProducts.winBuildProducts.ListGrade;
  var ListWidth = Aliases.CS32BitProxyHost.HwndSource_winBuildProducts.winBuildProducts.ListThickness;
  var ListLength = Aliases.CS32BitProxyHost.HwndSource_winBuildProducts.winBuildProducts.ListWidth;
  var btnAddToList = Aliases.CS32BitProxyHost.HwndSource_winBuildProducts.winBuildProducts.btnAddToList;
  //Plates
  var SelectionCosting = Aliases.CSDirector.HwndSource_MaterialCatalogDialog.MaterialCatalogDialog.radDocking.RadSplitContainer.NavigatorRadPanelGroup.MaterialNavTreeView.Plates.SSTPlates.SelectionCosting;
  var CostingConfiguration = Aliases.CSDirector.HwndSource_MaterialCatalogDialog.MaterialCatalogDialog.radDocking.RadSplitContainer.NavigatorRadPanelGroup.MaterialNavTreeView.Plates.SSTPlates.CostingConfiguration;
  
  //Grid
  var PlateCostGrid = Aliases.CSDirector.HwndSource_MaterialCatalogDialog.MaterialCatalogDialog.radDocking.RadSplitContainer1.WorkspacePaneGroup.PlateCostGrid;
  
 if (arg==1)
 { 
  SelectionCosting.Click();
  btnAddMaterial.Click();
  HwndSource_winProdPriority.WaitWindow(2000);
  btnAddProduct.ClickButton();
  
  ListType.ClickItem("AS",skCtrl);
  ListType.ClickItem("AS_HS",skCtrl);
  ListType.ClickItem("AS_S5",skCtrl);
  ListType.ClickItem("AS_S6",skCtrl);
  
  ListGauge.ClickItem("18",skCtrl);
  ListGauge.ClickItem("20",skCtrl);
  
  ListWidth.ClickItem(0);
  ListWidth.Keys("^a[Release]");
  
  ListLength.ClickItem(0);
  ListLength.Keys("^a[Release]");
  
  btnAddToList.ClickButton();
  aqUtils.Delay(10000);
  Aliases.CS32BitProxyHost.HwndSource_winBuildProducts.winBuildProducts.btnOk.ClickButton();
  
  Aliases.CS32BitProxyHost.HwndSource_winProdPriority.winProdPriority.btnOk.ClickButton();
  
  //****
  CostingConfiguration.Click();
  
  PlateCostGrid.ClickCell(0,2);
  PlateCostGrid.Keys("2");
  PlateCostGrid.ClickCell(1,2);
  PlateCostGrid.Keys("1");
  PlateCostGrid.ClickCell(2,2);
  PlateCostGrid.Keys("1");
  PlateCostGrid.ClickCell(3,2);
  PlateCostGrid.Keys("1");
  PlateCostGrid.ClickCell(0,2);
 }
 
 if (arg==2)
 {
  SelectionCosting.Click();
  btnAddMaterial.Click();
  HwndSource_winProdPriority.WaitWindow(2000);
  btnAddProduct.ClickButton();
  
  ListType.ClickItem("AS",skCtrl);
  ListType.ClickItem("AS_HS",skCtrl);
  
  ListGauge.ClickItem("18",skCtrl);
  ListGauge.ClickItem("20",skCtrl);
  
  ListWidth.ClickItem(0);
  ListWidth.Keys("^a[Release]");
  
  ListLength.ClickItem(0);
  ListLength.Keys("^a[Release]");
  
  btnAddToList.ClickButton();
  aqUtils.Delay(10000);

  Aliases.CS32BitProxyHost.HwndSource_winBuildProducts.winBuildProducts.btnOk.ClickButton();
  
  Aliases.CS32BitProxyHost.HwndSource_winProdPriority.winProdPriority.btnOk.ClickButton();
  
  //****
  CostingConfiguration.Click();
  
//  PlateCostGrid.ClickCell(0,2);
//  PlateCostGrid.Keys("2");
//  PlateCostGrid.ClickCell(1,2);
//  PlateCostGrid.Keys("1");
  PlateCostGrid.ClickCell(2,2);
  PlateCostGrid.Keys("2");
  PlateCostGrid.ClickCell(3,2);
  PlateCostGrid.Keys("1");
  PlateCostGrid.ClickCell(0,2);
  
 }
 if (arg==3)
 { 
  SelectionCosting.Click();
  btnAddMaterial.Click();
  HwndSource_winProdPriority.WaitWindow(2000);
  btnAddProduct.ClickButton();
  
  ListType.ClickItem("AS",skCtrl);
  ListType.ClickItem("AS_HS",skCtrl);
  ListType.ClickItem("AS_S5",skCtrl);
  ListType.ClickItem("AS_S6",skCtrl);
  
  ListGauge.ClickItem("18",skCtrl);
  ListGauge.ClickItem("20",skCtrl);
  
  ListWidth.ClickItem(0);
  ListWidth.Keys("^a[Release]");
  
  ListLength.ClickItem(0);
  ListLength.Keys("^a[Release]");
  
  btnAddToList.ClickButton();
  aqUtils.Delay(10000);
  Aliases.CS32BitProxyHost.HwndSource_winBuildProducts.winBuildProducts.btnOk.ClickButton();
  
  Aliases.CS32BitProxyHost.HwndSource_winProdPriority.winProdPriority.btnOk.ClickButton();
  
  //****
  CostingConfiguration.Click();
  
  PlateCostGrid.ClickCell(0,2);
  PlateCostGrid.Keys("2");
  PlateCostGrid.ClickCell(1,2);
  PlateCostGrid.Keys("1");
  PlateCostGrid.ClickCell(2,2);
  PlateCostGrid.Keys("1");
  PlateCostGrid.ClickCell(3,2);
  PlateCostGrid.Keys("1");
  PlateCostGrid.ClickCell(0,2);
 }
 
} 

function AddValueLumber (arg)
{
//Lumber
  var Selection = Aliases.CSDirector.HwndSource_MaterialCatalogDialog.MaterialCatalogDialog.radDocking.RadSplitContainer.NavigatorRadPanelGroup.MaterialNavTreeView.Lumber.Dimensional.Selection;
  var Costing = Aliases.CSDirector.HwndSource_MaterialCatalogDialog.MaterialCatalogDialog.radDocking.RadSplitContainer.NavigatorRadPanelGroup.MaterialNavTreeView.Lumber.Dimensional.Costing;

var btnAddMaterial = Aliases.CSDirector.HwndSource_MaterialCatalogDialog.MaterialCatalogDialog.RibbonMaterialInventoryItems.btnAddMaterial;
    //add product 
  var HwndSource_winProdPriority = Aliases.CS32BitProxyHost.HwndSource_winProdPriority;
  var btnAddProduct = Aliases.CS32BitProxyHost.HwndSource_winProdPriority.winProdPriority.btnAddProduct;

  var ListSpecies = Aliases.CS32BitProxyHost.HwndSource_winBuildProducts.winBuildProducts.ListSpecies;
  var ListGrade = Aliases.CS32BitProxyHost.HwndSource_winBuildProducts.winBuildProducts.ListGrade;
  var ListThickness = Aliases.CS32BitProxyHost.HwndSource_winBuildProducts.winBuildProducts.ListThickness;
  var ListWidth = Aliases.CS32BitProxyHost.HwndSource_winBuildProducts.winBuildProducts.ListWidth;
  var btnAddToList = Aliases.CS32BitProxyHost.HwndSource_winBuildProducts.winBuildProducts.btnAddToList;
  
  var LumberPriceGrid = Aliases.CSDirector.HwndSource_MaterialCatalogDialog.MaterialCatalogDialog.radDocking.RadSplitContainer1.WorkspacePaneGroup.LumberPriceGrid;
  
  //Structural Composite Lumber
  var StrSelection = Aliases.CSDirector.HwndSource_MaterialCatalogDialog.MaterialCatalogDialog.radDocking.RadSplitContainer.NavigatorRadPanelGroup.MaterialNavTreeView.Lumber.Structural.StrSelection;
  var cboProductTypes = Aliases.CS32BitProxyHost.HwndSource_winBuildProducts.winBuildProducts.cboProductTypes;
  
  //Trimmable End Inserts
  var TrimFitSelection = Aliases.CSDirector.HwndSource_MaterialCatalogDialog.MaterialCatalogDialog.radDocking.RadSplitContainer.NavigatorRadPanelGroup.MaterialNavTreeView.Lumber.Trimmable.TrimFitSelection;
  
  
  
  if (arg==1)
  {
    //Lumber
    Selection.Click();
    btnAddMaterial.Click();
    HwndSource_winProdPriority.WaitWindow(2000);
    btnAddProduct.ClickButton();
  
    ListSpecies.ClickItem("Douglas Fir-Larch (N)",skCtrl);
    ListSpecies.ClickItem("Hem-Fir",skCtrl);
    ListSpecies.ClickItem("Spruce-Pine-Fir",skCtrl);
  
    ListGrade.ClickItem(0);
    ListGrade.Keys("^a[Release]");
  
    ListThickness.ClickItem("2");
  
    ListWidth.ClickItem("4",skCtrl);
    ListWidth.ClickItem("6",skCtrl);
    ListWidth.ClickItem("8",skCtrl);
  
    btnAddToList.ClickButton();
  
    aqUtils.Delay(4000);
    Aliases.CS32BitProxyHost.HwndSource_winBuildProducts.winBuildProducts.btnOk.ClickButton();
  
    Aliases.CS32BitProxyHost.HwndSource_winProdPriority.winProdPriority.btnOk.ClickButton();
  
    Costing.Click();
    //Aliases.CSDirector.HwndSource_MaterialCatalogDialog.MaterialCatalogDialog.radDocking.RadSplitContainer1.WorkspacePaneGroup.GridviewgrouppanelitemSpeciesgrade.Click(111, 14);
  
    radPaneGroup = Aliases.CSDirector.HwndSource_MaterialCatalogDialog.MaterialCatalogDialog.radDocking.RadSplitContainer1.WorkspacePaneGroup;
    if (radPaneGroup.GridviewgrouppanelitemSpeciesgrade.Exists)
    {
    radPaneGroup.GridviewgrouppanelitemSpeciesgrade.Click(112, 13);  
    }
    if (radPaneGroup.GridviewgrouppanelitemSize.Exists) 
    {
    radPaneGroup.GridviewgrouppanelitemSize.Click(61, 12);  
    } 
  
    LumberPriceGrid.Keys("^a[Release]");
    LumberPriceGrid.ClickCell(0,5,skCtrl);
    LumberPriceGrid.Keys("1000[Enter]");
  }
  
  if (arg==2)
  {
    Selection.Click();
    btnAddMaterial.Click();
    HwndSource_winProdPriority.WaitWindow(2000);
    btnAddProduct.ClickButton();
    
    ListSpecies.ClickItem("Spruce-Pine-Fir");
    
    ListGrade.ClickItem("#2",skCtrl);
    ListGrade.ClickItem("#3",skCtrl);
    ListGrade.ClickItem("1650/1.5",skCtrl);
    ListGrade.ClickItem("2100/1.8",skCtrl);
    ListGrade.ClickItem("2400/2.0",skCtrl);
    
    ListThickness.ClickItem("2");

    ListWidth.ClickItem("4",skCtrl);
    ListWidth.ClickItem("6",skCtrl);
    ListWidth.ClickItem("8",skCtrl);
    ListWidth.ClickItem("10",skCtrl);
    
    btnAddToList.ClickButton();
  
    aqUtils.Delay(4000);
    Aliases.CS32BitProxyHost.HwndSource_winBuildProducts.winBuildProducts.btnOk.ClickButton();
  
    Aliases.CS32BitProxyHost.HwndSource_winProdPriority.winProdPriority.btnOk.ClickButton();
    
    Costing.Click();
    //Aliases.CSDirector.HwndSource_MaterialCatalogDialog.MaterialCatalogDialog.radDocking.RadSplitContainer1.WorkspacePaneGroup.GridviewgrouppanelitemSpeciesgrade.Click(111, 14);
  
    radPaneGroup = Aliases.CSDirector.HwndSource_MaterialCatalogDialog.MaterialCatalogDialog.radDocking.RadSplitContainer1.WorkspacePaneGroup;
    if (radPaneGroup.GridviewgrouppanelitemSpeciesgrade.Exists)
    {
    radPaneGroup.GridviewgrouppanelitemSpeciesgrade.Click(112, 13);  
    }
    if (radPaneGroup.GridviewgrouppanelitemSize.Exists) 
    {
    radPaneGroup.GridviewgrouppanelitemSize.Click(61, 12);  
    } 
  
    LumberPriceGrid.Keys("^a[Release]");
    LumberPriceGrid.ClickCell(0,5,skCtrl);
    LumberPriceGrid.Keys("1000[Enter]");
    
  }
  if(arg==3)
  {
    Selection.Click();
    btnAddMaterial.Click();
    HwndSource_winProdPriority.WaitWindow(2000);
    btnAddProduct.ClickButton();
    
    ListSpecies.ClickItem("Cottonwood",skCtrl);
    ListSpecies.ClickItem("Douglas Fir-Larch",skCtrl);
    ListSpecies.ClickItem("Hem-Fir",skCtrl);
    ListSpecies.ClickItem("Redwood",skCtrl);
    ListSpecies.ClickItem("Spruce-Pine-Fir",skCtrl);
    ListSpecies.ClickItem("SYP",skCtrl);
    
    ListGrade.ClickItem("#2",skCtrl);
    ListGrade.ClickItem("2400/2.0",skCtrl);
    
    ListThickness.ClickItem("2");
    
    ListWidth.ClickItem("4",skCtrl);
    ListWidth.ClickItem("6",skCtrl);
    ListWidth.ClickItem("8",skCtrl);

    btnAddToList.ClickButton();
  
    aqUtils.Delay(4000);
    Aliases.CS32BitProxyHost.HwndSource_winBuildProducts.winBuildProducts.btnOk.ClickButton();
  
    Aliases.CS32BitProxyHost.HwndSource_winProdPriority.winProdPriority.btnOk.ClickButton();
    
//Structural Composite
    StrSelection.Click();
    btnAddMaterial.Click();
    HwndSource_winProdPriority.WaitWindow(2000);
    btnAddProduct.ClickButton();

    //--=--    
    cboProductTypes.ClickItem(0);//Boise Cascade Versa 
    
    ListSpecies.ClickItem("VL 1.4 1800 DF",skCtrl);
    ListSpecies.ClickItem("VL 2.0 3100 SP",skCtrl);

    ListGrade.ClickItem("1 1/2",skCtrl);
    ListGrade.ClickItem("3 1/2",skCtrl);
    
    ListThickness.ClickItem("3 1/2",skCtrl);
    ListThickness.ClickItem("7 1/4",skCtrl);
    ListThickness.ClickItem("11 7/8",skCtrl);
    
    btnAddToList.ClickButton();
    
    //--=--
    cboProductTypes.ClickItem(1);//Broadspan LVL
    
    //ListSpecies.ClickItem("Broadspan 1.3E LVL",skCtrl);
    ListSpecies.ClickItem("Broadspan 2.0E LVL",skCtrl);

    ListGrade.ClickItem("1 1/2",skCtrl);
    ListGrade.ClickItem("3 1/2",skCtrl);
    
    ListThickness.ClickItem("3 1/2",skCtrl);
    ListThickness.ClickItem("5 1/2",skCtrl);
    ListThickness.ClickItem("11 7/8",skCtrl);
    
    btnAddToList.ClickButton();
    
     aqUtils.Delay(4000);
    Aliases.CS32BitProxyHost.HwndSource_winBuildProducts.winBuildProducts.btnOk.ClickButton();
  
    Aliases.CS32BitProxyHost.HwndSource_winProdPriority.winProdPriority.btnOk.ClickButton();
    
// Trimmable End Inserts
    TrimFitSelection.Click();
    btnAddMaterial.Click();
    HwndSource_winProdPriority.WaitWindow(2000);
    btnAddProduct.ClickButton();
   
    //--=--
    cboProductTypes.ClickItem(0);//EverTrim
    ListSpecies.ClickItem(0);
    ListSpecies.Keys("^a[Release]");
    
    btnAddToList.ClickButton();
    //--=--
    cboProductTypes.ClickItem(1);//Trim Fit
    ListSpecies.ClickItem(0);
    ListSpecies.Keys("^a[Release]");
    
    btnAddToList.ClickButton();
    
    aqUtils.Delay(4000);
    Aliases.CS32BitProxyHost.HwndSource_winBuildProducts.winBuildProducts.btnOk.ClickButton();
  
    Aliases.CS32BitProxyHost.HwndSource_winProdPriority.winProdPriority.btnOk.ClickButton();
     
  }
  
} 

//*****SawTable********

function SettingSawTable ()
{
    var PresetManagerWindow = Aliases.PresetManagerWindow.PresetManagerWindow;
    var SawTables = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonSettingsItems.SawTables;
   
    var SchemeListCombo = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.SchemeListCombo;
    
    var Add_Scheme_Button = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.Add_Scheme_Button;
    var btnAddSaw = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.btnAddSaw;
    var btnAddAssembly = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.btnAddAssembly;
    
      var ComboboxSawType = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.Tree_Control_.Saw_Cuts_Group_Box.Grid.Grid.Grid.ComboboxSawType; 
      var tbSawName = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.Grid.tbSawName;
      var tbMinAng = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.Tree_Control_.ItemsControl.tbMinAng;
      var tbMaxAng = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.Tree_Control_.ItemsControl.tbMaxAng;
      var tbMaxCut = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.Tree_Control_.ItemsControl.tbMaxCut;
      var tbKerf = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.Tree_Control_.ItemsControl.tbKerf;
      
      var tbMinFeedLength = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.Tree_Control_.Board_Properties_Group_Box.tbMinFeedLength;
      var tbMinPieceLength = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.Tree_Control_.Board_Properties_Group_Box.tbMinPieceLength;
      var tbMinBoardDepthExact = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.Tree_Control_.Board_Properties_Group_Box.tbMinBoardDepthExact;
      var tbMinBoardWidthExact = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.Tree_Control_.Board_Properties_Group_Box.tbMinBoardWidthExact;
      var tbMaxFeed = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.Tree_Control_.tbMaxFeed;
      var tbMaxPiece = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.Tree_Control_.tbMaxPiece;
      var tbMaxBoardDepth = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.Tree_Control_.tbMaxBoardDepth;
      var tbMaxBoardWidth = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.Tree_Control_.tbMaxBoardWidth;
      
      var CheckboxSingle = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.Tree_Control_.Row_Checkbox_Copy8.CheckboxSingle;
      var CheckboxDouble = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.Tree_Control_.Row_Checkbox_Copy.CheckboxDouble;
      
      var ComboboxFileType = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.Tree_Control_.File_Settings_Group_Box.ComboboxFileType;
      
      var chkNewLinePerTruss = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.Tree_Control_.File_Settings_Group_Box.chkNewLinePerTruss;
      
      var CheckboxLumber = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.Tree_Control_.Row_Checkbox_Copy13.CheckboxLumber;
      var CheckboxScl = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.Tree_Control_.Row_Checkbox_Copy15.CheckboxScl;
      var CheckboxIJoist = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.Tree_Control_.Row_Checkbox_Copy17.CheckboxIJoist;
      
      var tbMinNumberOfCuts = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.Tree_Control_.Cut_Type_Group_Box.StackPanel.StackPanel.tbMinNumberOfCuts;
      var tbMaxNumberOfCuts = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.Tree_Control_.Cut_Type_Group_Box.StackPanel.StackPanel2.tbMaxNumberOfCuts;
      var tbMaxNumberOfLeftCuts = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.Tree_Control_.Cut_Type_Group_Box.StackPanel.StackPanel3.tbMaxNumberOfLeftCuts;
      var tbMaxNumberOfRightCuts = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.Tree_Control_.Cut_Type_Group_Box.StackPanel.StackPanel4.tbMaxNumberOfRightCuts;
      
      var tbMinPieceCounts = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.Tree_Control_.Piece_Count_Group_Box.tbMinPieceCounts;
      var tbMaxPieceCounts = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.Tree_Control_.Piece_Count_Group_Box.tbMaxPieceCounts;
            
      var chkTopChords = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.Tree_Control_.Piece_Type_Group_box.WrapPanel.StackPanel.StackPanel.chkTopChords;
      var chkBottomChords = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.Tree_Control_.Piece_Type_Group_box.WrapPanel.StackPanel.StackPanel2.chkBottomChords;
      var chkStackedChords = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.Tree_Control_.Piece_Type_Group_box.WrapPanel.StackPanel.StackPanel3.chkStackedChords;
      var chkWebs = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.Tree_Control_.Piece_Type_Group_box.WrapPanel.StackPanel.StackPanel4.chkWebs;
      var chkGables = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.Tree_Control_.Piece_Type_Group_box.WrapPanel.StackPanel.StackPanel5.chkGables;
      var chkSliders = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.Tree_Control_.Piece_Type_Group_box.WrapPanel.StackPanel2.StackPanel.chkSliders;
      var chkWedges = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.Tree_Control_.Piece_Type_Group_box.WrapPanel.StackPanel2.StackPanel2.chkWedges;
      var chkScabs = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.Tree_Control_.Piece_Type_Group_box.WrapPanel.StackPanel2.StackPanel3.chkScabs;
      var chkNonStructuralPieces = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.Tree_Control_.Piece_Type_Group_box.WrapPanel.StackPanel2.StackPanel4.chkNonStructuralPieces;
    
    
    //*************BEGIN
    
    SawTables.Click(5,5);
    aqUtils.Delay(5000);
    //PresetManagerWindow.WaitWindow(2000);
    if (!PresetManagerWindow.Exists)
    {
      SawTables.Click(5,5);
    }
    
    SchemeListCombo.Keys("Hanson[Enter]");
    Aliases.PresetManagerWindow.dlgConfirm.btnYes.ClickButton();
    
  mainWindow = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow;
  
   while (Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.Shop_View_Scroll_Viewer.SawList.ContentPresenter.Exists)
   {
     mainWindow.Shop_View_Scroll_Viewer.SawList.ContentPresenter.Delete_Station_Button.ClickButton();
   } 
   while (Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.Shop_View_Scroll_Viewer.AssemblyList.ContentPresenter.Exists)
   {
     mainWindow.Shop_View_Scroll_Viewer.AssemblyList.ContentPresenter.Delete_Station_Button.ClickButton();
   } 
    
/*   
//*********Add Saw**********
   //++++++++++++++++++++Hund_Gable
   btnAddSaw.ClickButton();
   
   ComboboxSawType.ClickItem("Hundegger");
   tbSawName.SetText("Hund_Gable");
   tbMinAng.SetText("-360");
   tbMaxAng.SetText("360");
   tbMaxCut.SetText("60-0-0");
   tbKerf.SetText("0.25");
   
   tbMinFeedLength.SetText("0-0-0");
   tbMinPieceLength.SetText("0-0-0");
   tbMinBoardDepthExact.SetText("0-0-0");
   tbMinBoardWidthExact.SetText("0-0-0");
   tbMaxFeed.SetText("20-0-8");
   tbMaxPiece.SetText("20-0-8");
   tbMaxBoardDepth.SetText("0-11-4");
   tbMaxBoardWidth.SetText("0-11-4");
   
   CheckboxSingle.ClickButton(false);
   CheckboxDouble.ClickButton(false);
   
   ComboboxFileType.ClickItem("bvx");
   
   chkNewLinePerTruss.ClickButton(true);
   
   CheckboxLumber.ClickButton(true);
   CheckboxScl.ClickButton(true);
   CheckboxIJoist.ClickButton(false);
   
   tbMinNumberOfCuts.SetText("0");
   tbMaxNumberOfCuts.SetText("10");
   tbMaxNumberOfLeftCuts.SetText("3");
   tbMaxNumberOfRightCuts.SetText("3");
   
   tbMinPieceCounts.SetText("0");
   tbMaxPieceCounts.SetText("9999");
   
   chkTopChords.ClickButton(false);
   chkBottomChords.ClickButton(false);
   chkStackedChords.ClickButton(false);
   chkWebs.ClickButton(false);
   chkGables.ClickButton(true);
   chkSliders.ClickButton(false);
   chkWedges.ClickButton(false);
   chkScabs.ClickButton(true);
   chkNonStructuralPieces.ClickButton(false);
   
   //+++++++++++++++++++++++++Cyber_Chord
   btnAddSaw.ClickButton();
   
   ComboboxSawType.ClickItem("Cyber Easy-Set");
   tbSawName.SetText("Cyber_Chord");
   
   tbMinFeedLength.SetText("1-10-0");
   tbMinPieceLength.SetText("1-10-0");
   tbMinBoardDepthExact.SetText("0-1-8");
   tbMinBoardWidthExact.SetText("0-1-8");
   tbMaxFeed.SetText("20-0-8");
   tbMaxPiece.SetText("20-0-8");
   tbMaxBoardDepth.SetText("0-11-4");
   tbMaxBoardWidth.SetText("0-11-4");
   
   CheckboxSingle.ClickButton(false);
   CheckboxDouble.ClickButton(false);
   
   ComboboxFileType.ClickItem("ezy");
   
   chkNewLinePerTruss.ClickButton(true);
   
   CheckboxLumber.ClickButton(true);
   CheckboxScl.ClickButton(true);
   CheckboxIJoist.ClickButton(false);
   
   tbMinNumberOfCuts.SetText("0");
   tbMaxNumberOfCuts.SetText("10");
   tbMaxNumberOfLeftCuts.SetText("3");
   tbMaxNumberOfRightCuts.SetText("3")
   
   tbMinPieceCounts.SetText("0");
   tbMaxPieceCounts.SetText("9999");
   
   chkTopChords.ClickButton(true);
   chkBottomChords.ClickButton(true);
   chkStackedChords.ClickButton(false);
   chkWebs.ClickButton(false);
   chkGables.ClickButton(false);
   chkSliders.ClickButton(true);
   chkWedges.ClickButton(false);
   chkScabs.ClickButton(false);
   chkNonStructuralPieces.ClickButton(false);
   
    //+++++++++++++++++++++Cyber_Web
   btnAddSaw.ClickButton();
   
   ComboboxSawType.ClickItem("Cyber Easy-Set");
   tbSawName.SetText("Cyber_Web");
   
   tbMinFeedLength.SetText("1-10-0");
   tbMinPieceLength.SetText("1-10-0");
   tbMinBoardDepthExact.SetText("0-1-8");
   tbMinBoardWidthExact.SetText("0-1-8");
   tbMaxFeed.SetText("20-0-8");
   tbMaxPiece.SetText("20-0-8");
   tbMaxBoardDepth.SetText("0-11-4");
   tbMaxBoardWidth.SetText("0-11-4");
   
   CheckboxSingle.ClickButton(false);
   CheckboxDouble.ClickButton(false);
   
   ComboboxFileType.ClickItem("ezy");
   
   chkNewLinePerTruss.ClickButton(true);
   
   CheckboxLumber.ClickButton(true);
   CheckboxScl.ClickButton(true);
   CheckboxIJoist.ClickButton(false);
   
   tbMinNumberOfCuts.SetText("0");
   tbMaxNumberOfCuts.SetText("10");
   tbMaxNumberOfLeftCuts.SetText("3");
   tbMaxNumberOfRightCuts.SetText("3");
   
   tbMinPieceCounts.SetText("0");
   tbMaxPieceCounts.SetText("9999");
   
   chkTopChords.ClickButton(false);
   chkBottomChords.ClickButton(false);
   chkStackedChords.ClickButton(false);
   chkWebs.ClickButton(true);
   chkGables.ClickButton(false);
   chkSliders.ClickButton(true);
   chkWedges.ClickButton(false);
   chkScabs.ClickButton(true);
   chkNonStructuralPieces.ClickButton(true);
   
   
    //+++++++++++++++++++++Hund_Web
   btnAddSaw.ClickButton();
   
   ComboboxSawType.ClickItem("Hundegger");
   tbSawName.SetText("Hund_Web");
   tbMinAng.SetText("-360");
   tbMaxAng.SetText("360");
   tbMaxCut.SetText("60-0-0");
   tbKerf.SetText("0.25");
   
   tbMinFeedLength.SetText("0-0-0");
   tbMinPieceLength.SetText("0-1-4");
   tbMinBoardDepthExact.SetText("0-1-8");
   tbMinBoardWidthExact.SetText("0-1-8");
   tbMaxFeed.SetText("20-0-8");
   tbMaxPiece.SetText("20-0-8");
   tbMaxBoardDepth.SetText("0-11-4");
   tbMaxBoardWidth.SetText("0-11-4");
   
   CheckboxSingle.ClickButton(true);
   CheckboxDouble.ClickButton(true);
   
   ComboboxFileType.ClickItem("bvx");
   
   chkNewLinePerTruss.ClickButton(true);
   
   CheckboxLumber.ClickButton(true);
   CheckboxScl.ClickButton(true);
   CheckboxIJoist.ClickButton(false);
   
   tbMinNumberOfCuts.SetText("0");
   tbMaxNumberOfCuts.SetText("10");
   tbMaxNumberOfLeftCuts.SetText("3");
   tbMaxNumberOfRightCuts.SetText("3");
   
   tbMinPieceCounts.SetText("1");
   tbMaxPieceCounts.SetText("3");
   
   chkTopChords.ClickButton(false);
   chkBottomChords.ClickButton(false);
   chkStackedChords.ClickButton(false);
   chkWebs.ClickButton(true);
   chkGables.ClickButton(false);
   chkSliders.ClickButton(true);
   chkWedges.ClickButton(true);
   chkScabs.ClickButton(true);
   chkNonStructuralPieces.ClickButton(true);
   
   
   //+++++++++++++++++++++Hund_TCBev
   btnAddSaw.ClickButton();
   
   ComboboxSawType.ClickItem("Hundegger");
   tbSawName.SetText("Hund_TCBev");
   tbMinAng.SetText("-360");
   tbMaxAng.SetText("360");
   tbMaxCut.SetText("60-0-0");
   tbKerf.SetText("0.25");
   
   tbMinFeedLength.SetText("0-0-0");
   tbMinPieceLength.SetText("0-1-2");
   tbMinBoardDepthExact.SetText("0-1-8");
   tbMinBoardWidthExact.SetText("0-1-8");
   tbMaxFeed.SetText("20-0-8");
   tbMaxPiece.SetText("20-0-8");
   tbMaxBoardDepth.SetText("0-11-4");
   tbMaxBoardWidth.SetText("0-11-4");
   
   CheckboxSingle.ClickButton(false);
   CheckboxDouble.ClickButton(true);
   
   ComboboxFileType.ClickItem("bvx");
   
   chkNewLinePerTruss.ClickButton(false);
   
   CheckboxLumber.ClickButton(true);
   CheckboxScl.ClickButton(true);
   CheckboxIJoist.ClickButton(false);
   
   tbMinNumberOfCuts.SetText("0");
   tbMaxNumberOfCuts.SetText("10");
   tbMaxNumberOfLeftCuts.SetText("3");
   tbMaxNumberOfRightCuts.SetText("3");
   
   tbMinPieceCounts.SetText("0");
   tbMaxPieceCounts.SetText("9999");
   
   chkTopChords.ClickButton(true);
   chkBottomChords.ClickButton(true);
   chkStackedChords.ClickButton(false);
   chkWebs.ClickButton(false);
   chkGables.ClickButton(false);
   chkSliders.ClickButton(false);
   chkWedges.ClickButton(false);
   chkScabs.ClickButton(false);
   chkNonStructuralPieces.ClickButton(false);
   
   //+++++++++++++++++++++Monet
   btnAddSaw.ClickButton();
   
   ComboboxSawType.ClickItem("Non-Automated");
   tbSawName.SetText("Monet");
   tbMinAng.SetText("-360");
   tbMaxAng.SetText("360");
   tbMaxCut.SetText("20-0-0");
   tbKerf.SetText("0.25");
   
   tbMinFeedLength.SetText("0-0-0");
   tbMinPieceLength.SetText("0-0-0");
   tbMinBoardDepthExact.SetText("0-1-8");
   tbMinBoardWidthExact.SetText("0-1-8");
   tbMaxFeed.SetText("20-0-8");
   tbMaxPiece.SetText("20-0-8");
   tbMaxBoardDepth.SetText("0-11-4");
   tbMaxBoardWidth.SetText("0-11-4");
   
   CheckboxSingle.ClickButton(false);
   CheckboxDouble.ClickButton(false);
   
   //ComboboxFileType.ClickItem("bvx");
   
   chkNewLinePerTruss.ClickButton(true);
   
   CheckboxLumber.ClickButton(true);
   CheckboxScl.ClickButton(true);
   CheckboxIJoist.ClickButton(false);
   
   tbMinNumberOfCuts.SetText("0");
   tbMaxNumberOfCuts.SetText("10");
   tbMaxNumberOfLeftCuts.SetText("3");
   tbMaxNumberOfRightCuts.SetText("3");
   
   tbMinPieceCounts.SetText("0");
   tbMaxPieceCounts.SetText("999");
   
   chkTopChords.ClickButton(false);
   chkBottomChords.ClickButton(false);
   chkStackedChords.ClickButton(false);
   chkWebs.ClickButton(false);
   chkGables.ClickButton(false);
   chkSliders.ClickButton(false);
   chkWedges.ClickButton(false);
   chkScabs.ClickButton(false);
   chkNonStructuralPieces.ClickButton(false);
   
   //+++++++++++++++++++++Metra
   btnAddSaw.ClickButton();
   
   ComboboxSawType.ClickItem("Non-Automated");
   tbSawName.SetText("Metra");
   tbMinAng.SetText("-360");
   tbMaxAng.SetText("360");
   tbMaxCut.SetText("20-0-0");
   tbKerf.SetText("0.25");
   
   tbMinFeedLength.SetText("0-0-0");
   tbMinPieceLength.SetText("0-0-0");
   tbMinBoardDepthExact.SetText("0-1-8");
   tbMinBoardWidthExact.SetText("0-1-8");
   tbMaxFeed.SetText("20-0-8");
   tbMaxPiece.SetText("20-0-8");
   tbMaxBoardDepth.SetText("0-11-4");
   tbMaxBoardWidth.SetText("0-11-4");
   
   CheckboxSingle.ClickButton(true);
   CheckboxDouble.ClickButton(true);
   
   //ComboboxFileType.ClickItem("bvx");
   
   chkNewLinePerTruss.ClickButton(false);
   
   CheckboxLumber.ClickButton(true);
   CheckboxScl.ClickButton(true);
   CheckboxIJoist.ClickButton(true);
   
   tbMinNumberOfCuts.SetText("0");
   tbMaxNumberOfCuts.SetText("10");
   tbMaxNumberOfLeftCuts.SetText("3");
   tbMaxNumberOfRightCuts.SetText("3");
   
   tbMinPieceCounts.SetText("0");
   tbMaxPieceCounts.SetText("999");
   
   chkTopChords.ClickButton(true);
   chkBottomChords.ClickButton(true);
   chkStackedChords.ClickButton(true);
   chkWebs.ClickButton(true);
   chkGables.ClickButton(true);
   chkSliders.ClickButton(true);
   chkWedges.ClickButton(true);
   chkScabs.ClickButton(true);
   chkNonStructuralPieces.ClickButton(true);

//********Add Assembly**********
   //btnAddAssembly.ClickButton();
  
  
//*******************ADD NEW SCHEME Chords_Webs*************
   Add_Scheme_Button.ClickButton();
  
   SchemeListCombo.Keys("Chords_Webs[Enter]");
    Aliases.PresetManagerWindow.dlgConfirm.btnYes.ClickButton();
    
  mainWindow = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow;
  
   while (Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.Shop_View_Scroll_Viewer.SawList.ContentPresenter.Exists)
   {
     mainWindow.Shop_View_Scroll_Viewer.SawList.ContentPresenter.Delete_Station_Button.ClickButton();
   } 
   while (Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.Shop_View_Scroll_Viewer.AssemblyList.ContentPresenter.Exists)
   {
     mainWindow.Shop_View_Scroll_Viewer.AssemblyList.ContentPresenter.Delete_Station_Button.ClickButton();
   } 
   
   //++++++++++++++++++++Chords
   btnAddSaw.ClickButton();
   
   ComboboxSawType.ClickItem("Non-Automated");
   tbSawName.SetText("Chords");
   tbMinAng.SetText("-360");
   tbMaxAng.SetText("360");
   tbMaxCut.SetText("60-0-0");
   tbKerf.SetText("0.25");
   
   tbMinFeedLength.SetText("0-0-0");
   tbMinPieceLength.SetText("0-0-0");
   tbMinBoardDepthExact.SetText("0-0-0");
   tbMinBoardWidthExact.SetText("0-0-0");
   tbMaxFeed.SetText("999-0-0");
   tbMaxPiece.SetText("999-0-0");
   tbMaxBoardDepth.SetText("999-0-0");
   tbMaxBoardWidth.SetText("999-0-0");
   
   CheckboxSingle.ClickButton(true);
   CheckboxDouble.ClickButton(true);
   
   //ComboboxFileType.ClickItem("bvx");
   
   chkNewLinePerTruss.ClickButton(true);
   
   CheckboxLumber.ClickButton(true);
   CheckboxScl.ClickButton(true);
   CheckboxIJoist.ClickButton(true);
   
   tbMinNumberOfCuts.SetText("0");
   tbMaxNumberOfCuts.SetText("10");
   tbMaxNumberOfLeftCuts.SetText("3");
   tbMaxNumberOfRightCuts.SetText("3");
   
   tbMinPieceCounts.SetText("0");
   tbMaxPieceCounts.SetText("9999");
   
   chkTopChords.ClickButton(true);
   chkBottomChords.ClickButton(true);
   chkStackedChords.ClickButton(true);
   chkWebs.ClickButton(false);
   chkGables.ClickButton(false);
   chkSliders.ClickButton(false);
   chkWedges.ClickButton(false);
   chkScabs.ClickButton(true);
   chkNonStructuralPieces.ClickButton(false);
   
   //++++++++++++++++++++Webs
   btnAddSaw.ClickButton();
   
   ComboboxSawType.ClickItem("Non-Automated");
   tbSawName.SetText("Webs");
   tbMinAng.SetText("-360");
   tbMaxAng.SetText("360");
   tbMaxCut.SetText("60-0-0");
   tbKerf.SetText("0.25");
   
   tbMinFeedLength.SetText("0-0-0");
   tbMinPieceLength.SetText("0-0-0");
   tbMinBoardDepthExact.SetText("0-0-0");
   tbMinBoardWidthExact.SetText("0-0-0");
   tbMaxFeed.SetText("999-0-0");
   tbMaxPiece.SetText("999-0-0");
   tbMaxBoardDepth.SetText("999-0-0");
   tbMaxBoardWidth.SetText("999-0-0");
   
   CheckboxSingle.ClickButton(true);
   CheckboxDouble.ClickButton(true);
   
   //ComboboxFileType.ClickItem("bvx");
   
   chkNewLinePerTruss.ClickButton(true);
   
   CheckboxLumber.ClickButton(true);
   CheckboxScl.ClickButton(true);
   CheckboxIJoist.ClickButton(true);
   
   tbMinNumberOfCuts.SetText("0");
   tbMaxNumberOfCuts.SetText("10");
   tbMaxNumberOfLeftCuts.SetText("3");
   tbMaxNumberOfRightCuts.SetText("3");
   
   tbMinPieceCounts.SetText("0");
   tbMaxPieceCounts.SetText("9999");
   
   chkTopChords.ClickButton(false);
   chkBottomChords.ClickButton(false);
   chkStackedChords.ClickButton(false);
   chkWebs.ClickButton(true);
   chkGables.ClickButton(true);
   chkSliders.ClickButton(true);
   chkWedges.ClickButton(true);
   chkScabs.ClickButton(false);
   chkNonStructuralPieces.ClickButton(true);
   
*/   
//*******************ADD NEW SCHEME Hundegger*************
   Add_Scheme_Button.ClickButton();
  
   SchemeListCombo.Keys("Hundegger[Enter]");
    Aliases.PresetManagerWindow.dlgConfirm.btnYes.ClickButton();
    
  mainWindow = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow;
  
   while (Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.Shop_View_Scroll_Viewer.SawList.ContentPresenter.Exists)
   {
     mainWindow.Shop_View_Scroll_Viewer.SawList.ContentPresenter.Delete_Station_Button.ClickButton();
   } 
   while (Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.Shop_View_Scroll_Viewer.AssemblyList.ContentPresenter.Exists)
   {
     mainWindow.Shop_View_Scroll_Viewer.AssemblyList.ContentPresenter.Delete_Station_Button.ClickButton();
   } 
   
   //++++++++++++++++++++Chords
   btnAddSaw.ClickButton();
   
   ComboboxSawType.ClickItem("Hundegger");
   tbSawName.SetText("Hundegger");
   tbMinAng.SetText("-360");
   tbMaxAng.SetText("360");
   tbMaxCut.SetText("60-0-0");
   tbKerf.SetText("0.25");
   
   Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.CancelButton.ClickButton();
} 


//*****Prese********
function SettingPreset()
{
  var PresetManagerWindow = Aliases.PresetManagerWindow.PresetManagerWindow;
  var Presets = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonSettingsItems.Presets;
  
  var TemplateCombo = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.TemplateCombo;
  
  var CSDGeneral = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.PresetTreeView.RadTreeViewItem.CSDGeneral;
  var RdCSBuild = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.RdCSBuild;
  var RdCSTruss = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.RdCSTruss;
  
  var CSDPricing = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.PresetTreeView.RadTreeViewItem.CSDPricing;
  var DollarsPerText = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.DollarsPerText;
  
  var CSDPieceImages = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.PresetTreeView.RadTreeViewItem.CSDPieceImages;
  var CheckboxShowPahDimension = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.CheckboxShowPahDimension;
  var CheckboxShowCumulativeVerticalDimensions = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.CheckboxShowCumulativeVerticalDimensions;
  var CheckboxShowPlumbCutDimensions = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.CheckboxShowPlumbCutDimensions;
  var CheckboxShowTopChordHeelToPeakDimensions = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.CheckboxShowTopChordHeelToPeakDimensions;
  
  
  var FabPieceLabeling = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.PresetTreeView.RadTreeViewItem2.FabPieceLabeling;
  var RdStandardTBW = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.RdStandardTBW;
  var RdCustomPrefix = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.RdCustomPrefix;
  var RdStartOver = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.ContentScroller.RdStartOver;
  var RdEachTruss = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.RdEachTruss;
  var RdSEparatePieces = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.RdSEparatePieces;
  var RdStandardCallout = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.RdStandardCallout;
  var RdStandardCalloutO = Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.RdStandardCalloutO;
  

  Presets.Click(5,5);
  PresetManagerWindow.WaitWindow(2000);
  if (!PresetManagerWindow.Exists)
  {
  Presets.Click(5,5);
  }
 
/*   
//****  
//+++++++SAVE TEMPLATE LAKESIDE  
  if (!aqString.StrMatches("Lakeside-Office",TemplateCombo.wItemList))
  {
      presetManagerWindow = Aliases.PresetManagerWindow;
      presetManagerWindow.PresetManagerWindow.MainWindow.radMenu.RadmenuitemFile.Click(13, 10);
      presetManagerWindow.HwndSource_PopupRoot.PopupRoot.SaveAs.Click(58, 13);

      var tbSaveAs = Aliases.PresetManagerWindow.HwndSource_winSaveAsTemplate.winSaveAsTemplate.tbSaveAs;
      var SaveAsTemplateButton = Aliases.PresetManagerWindow.HwndSource_winSaveAsTemplate.winSaveAsTemplate.SaveAsTemplateButton;
      tbSaveAs.Keys("Lakeside-Office");
      SaveAsTemplateButton.ClickButton();
  
      Aliases.PresetManagerWindow.dlgConfirm.btnYes.ClickButton();
  }
  else
  {
    TemplateCombo.ClickItem("Lakeside-Office");
    Aliases.PresetManagerWindow.dlgConfirm.btnYes.ClickButton();
  } 
  
  
  //CS Director - General
  CSDGeneral.Click();
  RdCSTruss.Click();
  
  //CS Director - Pricing
  CSDPricing.Click();
  DollarsPerText.Keys("333");
  
  //CS Director - Piece Images
  CSDPieceImages.Click();
  CheckboxShowPahDimension.ClickButton(true);
  CheckboxShowCumulativeVerticalDimensions.ClickButton(true);
  CheckboxShowPlumbCutDimensions.ClickButton(true);
  CheckboxShowTopChordHeelToPeakDimensions.ClickButton(true);
  
  FabPieceLabeling.Click();
  RdStandardTBW.Click();
  RdEachTruss.Click();
  RdSEparatePieces.Click();
  RdStandardCallout.Click();
  RdStandardCalloutO.Click();
*/  
//+++++++SAVE TEMPLATE STUDIO 

  if (!aqString.StrMatches("Studio",TemplateCombo.wItemList))
  {
      presetManagerWindow = Aliases.PresetManagerWindow;
      presetManagerWindow.PresetManagerWindow.MainWindow.radMenu.RadmenuitemFile.Click(13, 10);
      presetManagerWindow.HwndSource_PopupRoot.PopupRoot.SaveAs.Click(58, 13);

      var tbSaveAs = Aliases.PresetManagerWindow.HwndSource_winSaveAsTemplate.winSaveAsTemplate.tbSaveAs;
      var SaveAsTemplateButton = Aliases.PresetManagerWindow.HwndSource_winSaveAsTemplate.winSaveAsTemplate.SaveAsTemplateButton;
      tbSaveAs.Keys("Studio");
      SaveAsTemplateButton.ClickButton();
  
      Aliases.PresetManagerWindow.dlgConfirm.btnYes.ClickButton();
  }
  else
  {
    TemplateCombo.ClickItem("Studio");
    Aliases.PresetManagerWindow.dlgConfirm.btnYes.ClickButton();
  }
  
  //CS Director - General
  CSDGeneral.Click();
  RdCSTruss.Click();
  
    //CS Director - Pricing
  CSDPricing.Click();
  DollarsPerText.Keys("0");
  
  //CS Director - Piece Images
  CSDPieceImages.Click();
  CheckboxShowPahDimension.ClickButton(true);
  CheckboxShowCumulativeVerticalDimensions.ClickButton(true);
  CheckboxShowPlumbCutDimensions.ClickButton(true);
  CheckboxShowTopChordHeelToPeakDimensions.ClickButton(true);
  
  FabPieceLabeling.Click();
  RdStandardTBW.Click();
  RdStartOver.Click();
  
  
//+++++++SAVE TEMPLATE BUILDTRUSS 

  if (!aqString.StrMatches("BuildTruss",TemplateCombo.wItemList))
  {
      presetManagerWindow = Aliases.PresetManagerWindow;
      presetManagerWindow.PresetManagerWindow.MainWindow.radMenu.RadmenuitemFile.Click(13, 10);
      presetManagerWindow.HwndSource_PopupRoot.PopupRoot.SaveAs.Click(58, 13);

      var tbSaveAs = Aliases.PresetManagerWindow.HwndSource_winSaveAsTemplate.winSaveAsTemplate.tbSaveAs;
      var SaveAsTemplateButton = Aliases.PresetManagerWindow.HwndSource_winSaveAsTemplate.winSaveAsTemplate.SaveAsTemplateButton;
      tbSaveAs.Keys("BuildTruss");
      SaveAsTemplateButton.ClickButton();
  
      Aliases.PresetManagerWindow.dlgConfirm.btnYes.ClickButton();
  }
  else
  {
    TemplateCombo.ClickItem("BuildTruss");
    Aliases.PresetManagerWindow.dlgConfirm.btnYes.ClickButton();
  }
  
  //CS Director - General
  CSDGeneral.Click();
  RdCSBuild.Click();
  
  //CS Director - Piece Images
  CSDPieceImages.Click();
  CheckboxShowPahDimension.ClickButton(true);
  CheckboxShowCumulativeVerticalDimensions.ClickButton(true);
  CheckboxShowPlumbCutDimensions.ClickButton(true);
  CheckboxShowTopChordHeelToPeakDimensions.ClickButton(true);
  
  //CS Director - Piece Labeling
  FabPieceLabeling.Click();
  RdCustomPrefix.Click();
  RdStartOver.Click();
  
  
  Aliases.PresetManagerWindow.PresetManagerWindow.MainWindow.CancelButton.ClickButton();
  
} 

function Test1()
{
  var afxFrameOrView120u;
  afxFrameOrView120u = Aliases.TrussStudio.FormTrussStudio.MDIClient.wndAfx.AfxMDIFrame120u.AfxFrameOrView120u;
  afxFrameOrView120u.HScroll.Pos = 665;
  afxFrameOrView120u.VScroll.Pos = 1052;
  afxFrameOrView120u.Click(346, 232);
}

function Test2()
{
  var afxFrameOrView120u;
  afxFrameOrView120u = Aliases.TrussStudio.FormTrussStudio.MDIClient.wndAfx.AfxMDIFrame120u.AfxFrameOrView120u;
  afxFrameOrView120u.VScroll.Pos = 379;
  afxFrameOrView120u.Click(758, 139);
  afxFrameOrView120u.Click(973, 252);
  afxFrameOrView120u.Click(575, 426);
  afxFrameOrView120u.Click(309, 221);
  afxFrameOrView120u.ClickR(297, 216);
}

function Test3()
{
  var afxFrameOrView120u;
  afxFrameOrView120u = Aliases.TrussStudio.FormTrussStudio.MDIClient.wndAfx.AfxMDIFrame120u.AfxFrameOrView120u;
  afxFrameOrView120u.VScroll.Pos = 379;
  afxFrameOrView120u.Click(968, 260);
  afxFrameOrView120u.Click(880, 432);
  afxFrameOrView120u.Click(856, 137);
  afxFrameOrView120u.ClickR(917, 302);
}

function Test4()
{
  var afxFrameOrView120u;
  afxFrameOrView120u = Aliases.TrussStudio.FormTrussStudio.MDIClient.wndAfx.AfxMDIFrame120u.AfxFrameOrView120u;
  afxFrameOrView120u.VScroll.Pos = 379;
  afxFrameOrView120u.Click(560, 430);
  afxFrameOrView120u.Click(316, 344);
  afxFrameOrView120u.Click(786, 139);
  afxFrameOrView120u.Click(970, 364);
  afxFrameOrView120u.ClickR(608, 382);
}

function Test5()
{
  var afxFrameOrView120u;
  afxFrameOrView120u = Aliases.TrussStudio.FormTrussStudio.MDIClient.wndAfx.AfxMDIFrame120u.AfxFrameOrView120u;
  afxFrameOrView120u.HScroll.Pos = 665;
  afxFrameOrView120u.VScroll.Pos = 1052;
  afxFrameOrView120u.Click(788, 133);
  afxFrameOrView120u.Click(955, 325);
  afxFrameOrView120u.Click(575, 419);
  afxFrameOrView120u.Click(312, 183);
  afxFrameOrView120u.ClickR(303, 186);
}

function Test6()
{
  var afxFrameOrView120u;
  afxFrameOrView120u = Aliases.TrussStudio.FormTrussStudio.MDIClient.wndAfx.AfxMDIFrame120u.AfxFrameOrView120u;
  afxFrameOrView120u.HScroll.Pos = 665;
  afxFrameOrView120u.VScroll.Pos = 1052;
  afxFrameOrView120u.Click(967, 334);
  afxFrameOrView120u.Click(736, 421);
  afxFrameOrView120u.Click(743, 138);
  afxFrameOrView120u.ClickR(743, 137);
}


function Test7()
{
  var afxFrameOrView120u;
  afxFrameOrView120u = Aliases.TrussStudio.FormTrussStudio.MDIClient.wndAfx.AfxMDIFrame120u.AfxFrameOrView120u;
  afxFrameOrView120u.HScroll.Pos = 665;
  afxFrameOrView120u.VScroll.Pos = 1052;
  afxFrameOrView120u.Click(700, 411);
  afxFrameOrView120u.Click(313, 323);
  afxFrameOrView120u.Click(778, 140);
  afxFrameOrView120u.Click(956, 355);
  afxFrameOrView120u.ClickR(956, 355);
}