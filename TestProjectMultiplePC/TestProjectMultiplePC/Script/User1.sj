﻿//USEUNIT EditProject
//USEUNIT UtilitiesForApps

function Main ()
{
  // 1. Login
    performanceLog("S","1");
    LoginDirector("lnguyen1","lnguyen1");   
    performanceLog("E","1","Log in");
    //SaveExcelFile("2","3","x");
    User1();
}

function test()
{
  Log.Message(Project.Path)
}
function User1()
{
	// Init variables 
	var PlanElevations_Btn 	=		Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.RadTreeMenu.RadtreeviewitemPlansElevations;
	var ElevationA					= 	Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.PlansRadTreeView.RadTreeViewItem.RadtreeviewitemElevationA;
	var RunCSTruss_Btn			= 	Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonElevationItems.RunTitusBuild;
	var i										= 	0;
	var CheckOutBy				 	= 	Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.vwCSProjectList.Grid.RadGridViewProjects.GridViewRow.GridviewcellCheckOutBy;
	// Test Step: 
		
			 
		// 2.	Create Project
			 CreateProject("Project1","Create Project"); 
		// 3. Open layout
		   PlanElevations_Btn.Click();
			 ElevationA.Click();
			 performanceLog("S","1");
			 RunCSTruss_Btn.Click();
			 
	var TrussApp						= 	Aliases.TrussStudio.FormTrussStudio;
	var LayoutCommand				= 	Aliases.TrussStudio.FormTrussStudio.MDIClient.wndAfx.AfxMDIFrame140u.page32770.editLayoutCommands;
	var MenuBar_Layout			= 	Aliases.TrussStudio.FormTrussStudio.BCGPDockBarc0000081000510.toolbarMenuBar;
	var wnd 								= 	Aliases.TrussStudio.wndAfx.Item;
	var ProjectList					= 	Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer.NavigatorRadPanelGroup.NavTreeView.Administration.OurCompany;
	
		//	4. Create Layout P1 
			 if (TrussApp.WaitProperty("Exists", true, 20000))
				{		
					performanceLog("E","1","Open Layout");	
					aqUtils.Delay(15000);			
					LayoutCommand_Input(LayoutCommand,"r50\nd30\nl50\nc");
					LayoutCommand_Input(LayoutCommand,"visible all rp");
					LayoutCommand_Input(LayoutCommand,"begincut roof 200704 -30112 170911  200704 -30112 -911\ncut roof -10904 -140910 170911  -10904 -140910 -911  Y Y\ncut roof 231007 -260301 170911  231007 -260301 -911  Y Y\ncut roof 520214 -181115 170911  520214 -181115 -911  Y Y\nendcut");
					LayoutCommand_Input(LayoutCommand,"begincut roof 250013 -250604 290605  250013 -250604 -10402\ncut roof -11111 -220205 290605  -11111 -220205 -10402  Y Y\ncut roof 240207 -20415 290605  240207 -20415 -10402  Y Y\ncut roof 511001 -231005 290605  511001 -231005 -10402  Y Y\nendcut");
					LayoutCommand_Input(LayoutCommand,"clear rl slc sel pptc ppbc pftc pfbc\nclear rl slc sel pptc ppbc pftc pfbc");
					LayoutCommand_Input(LayoutCommand,"select ptl -214 -80414 290605  -214 -80414 -10402\nrl 0 -380000 0  0 80000 0\nnx 20000 108 1 1 1 0 0 Truss n n n dir 500201 -100801 0  500201 -100801 0  ta pa");
					LayoutCommand_Input(LayoutCommand,"clear rl slc sel pptc ppbc pftc pfbc");
					LayoutCommand_Input(LayoutCommand,"SetPresetLabelState \"T01\" \"T##\" LabelSame false 1\nAssignPresetLabels RubberBandBoxSelect \"T01\" \"T##\" LabelSame false 1 -30407 51112 290605 540904 -340602 290605 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26");
				}
				else 
					Log.Error("Layout no open");
					
		// 5. Click Analyze all truss
				MenuBar_Layout.ClickItem(9, true);
				wnd.ClickItem("Analyze All Trusses", false);
		// 6. Save and close layout		
				performanceLog("S","1");
				MenuBar_Layout.ClickItem("*File*", true);
				wnd.ClickItem("&Save\tCtrl+S", false);
				MenuBar_Layout.ClickItem("*File*", true);
				wnd.ClickItem("E&xit", false);
				performanceLog("E","1","Close and save Layout");
				aqUtils.Delay(15000);
		// 7. Checkin project to server
				performanceLog("S","1");
				ProjectList.Click();
				CheckInProject("Remove","OK");
				performanceLog("E","1","Check in project Project1");
		// Write step7 status
				WriteTextToFile(Project.Path+"User1.txt","step7",0,0);
				
		// 8. Refresh to project - Wait for User 2
			// Get status run to excel
					WaitToRun(0,"User2","step8");
			// Find Project 
					FindProject("Project1")
			// Put Log performance
				performanceLog("S","1");
			// Click refresh and check status 
				RefreshProjects();
				performanceLog("E","1","Refresh project at step 8");
				
				
		// 12. Refresh to project - Wait for User 2
				WaitToRun(1,"User2","step11");
				// Find Project 
				FindProject("Project1")
			// Put Log performance
				performanceLog("S","1");
			// Click refresh and check status 
				//RefreshWaitToCheck("Lan Tran");
				RefreshProjects();
				performanceLog("E","1","Refresh project at step 12");
				aqUtils.Delay(15000);
		// 13. Refresh to project - Wait for User 2
				//WaitToRun(2,"User2","step12");
				performanceLog("S","1");
				RefreshProjects();
				performanceLog("E","1","Refresh project at step 13");
				aqUtils.Delay(15000);
		// 15. Refresh to project
				WaitToRun(0,"All","e");
				performanceLog("S","1");
				RefreshProjects();
				performanceLog("E","1","Refresh project at step 15");
	
}

//------ LayoutCommand_Input function -------//
function LayoutCommand_Input(object,command)
{
		object.SetText(command);
		object.Keys("[End]")
		object.Keys("[Enter]");
}

//------ WaitToRun function -------//
//function WaitToRun(row)
//{
//	var i = 0;
//	
//	var Arr = ODT.Classes.NewArray();
//	Arr=ReadDataFromFile("S:\\@QA\\@AutomaionServer\\TestComplete\\TestProjectMultiplePC\\TestProjectMultiplePC\\All.txt");
//	var a = Arr.Items(row);
//	// Waiting for status update
//				while (a != "all")
//				{
//					i++;
//					aqUtils.Delay(1000);
//					Arr=ReadDataFromFile("S:\\@QA\\@AutomaionServer\\TestComplete\\TestProjectMultiplePC\\TestProjectMultiplePC\\All.txt");	
//					a = Arr.Items(row);
//				}
//}

//------ RefreshWaitToCheck function -------//
function RefreshWaitToCheck(CheckValue)
{			
	// Init variable
  var GridviewcellCheckOutBy = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.vwCSProjectList.Grid.RadGridViewProjects.GridViewRow.GridviewcellCheckOutBy;
  var i = 0;
	// Action 
	// Click RefreshProjects
	RefreshProjects();
	// Waiting for staus of Check Out By column update
  while (GridviewcellCheckOutBy.WPFControlText != CheckValue)
  {
			i++;
			aqUtils.Delay(2000);
  		RefreshProjects();  
  } 
}
