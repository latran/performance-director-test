﻿//USEUNIT UtilitiesForApps 
//USEUNIT EditProject
var FormMain = UserForms.FormTS_AUTOMATION;

function Main ()
{
    performanceLog("S","2");
    LoginDirector("latran","latran");   
    //SaveExcelFile("2","3","x");
    performanceLog("E","2","Log In");
    RunMain();
}

function test()
{
  WriteTextToFile("S:\\@QA\\@AutomaionServer\\TestComplete\\TestProjectMultiplePC\\TestProjectMultiplePC\\User2.txt","step8","0","0");
}

function RunMain()
{
  var GridViewRow = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.vwCSProjectList.Grid.RadGridViewProjects.GridViewRow;
  var RadtreeviewitemT02  = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.PlansRadTreeView.RadTreeViewItem.RadtreeviewitemElevationA.RadTreeViewItem;
  var RadtreeviewitemPlansElevations = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.RadTreeMenu.RadtreeviewitemPlansElevations;
  var ListviewitemProjectList = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.LvwOutlookMenu.ListviewitemProjectList;
  
  var Edit = Aliases.TrussStudio.FormTS.Item.wndAfx.AfxMDIFrame140u.page327702.Edit;
  var btnRun = Aliases.TrussStudio.FormTS.Item.wndAfx.AfxMDIFrame140u.page327702.btnRun;
  
	// Step 4 Check out project Test3, Test4
	// Check out project Test3
	aqUtils.Delay(60000);
	FindProject("Test3");
	GridViewRow.Click();
	performanceLog("S","2");
	CheckOutProject("Checked","ForEdit","OK")
	performanceLog("E","2","Check out project Test3");
	// Check out project Test4
	FindProject("Test4");
	GridViewRow.Click();
	performanceLog("S","2");
	CheckOutProject("Checked","ForEdit","OK")
	performanceLog("E","2","Check out project Test4");
	
  //step 8
  WaitToRun(0,"User1","step7");
	
  FindProject("Project1");
  GridViewRow.Click();
	performanceLog("S","2");
  CheckOutProject("Checked","ForEdit","OK");
	performanceLog("E","2","Check out project Project1");
  WriteTextToFile(Project.Path+"User2.txt","step8",0,0)
  
  
  GridViewRow.DblClick();
  
  //step 9
  RadtreeviewitemPlansElevations.Click();
  Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.PlansRadTreeView.RadTreeViewItem.RadtreeviewitemElevationA.Click(28, 14);
  RadtreeviewitemT02.DblClick();
  
  aqUtils.Delay(15000);
  
  Aliases.TrussStudio.FormTS.Item.wndAfx.AfxMDIFrame140u.page32770.BCGPTabWnd2c000081000310.Click(86, 240);
  
  //step 10
  Edit.SetText("3");Edit.Keys("[Tab][Release]");
  
  btnRun.ClickButton();
  
  aqUtils.Delay(1000);
  
  Aliases.TrussStudio.FormTS.Keys("^s[Release]");
  Aliases.TrussStudio.FormTS.Keys("~fx[Release]");

  
  //step 11
  
  Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.HomeRibbonTab.Click();
  performanceLog("S","2");
  CheckInProject("Remove","OK");
  performanceLog("E","2","Check in project Project1");
  WriteTextToFile(Project.Path+"User2.txt","step11",1,0);
  
  //step 12
  ListviewitemProjectList.Click();
  
  FindProject("Project1");
  
  GridViewRow.Click();
  
  ChangeStatus(false,true,false,false,"Hieu Do");
  ChangeStatus(false,false,true,false,"11/26/2019");
  ChangeStatus(false,false,false,true,"11/26/2019");
  
  WriteTextToFile(Project.Path+"User2.txt","step12",2,0);
  
  
  WaitToRun (0,"User3","step13");  
  performanceLog("S","2");
  RefreshProjects();
  
  performanceLog("E","2","Refresh and Check status");
  
  // Step 15 Check in multiple project at the same time with other users
  WaitToRun(0,"All","e"); 
  performanceLog("S","2");
  CheckInMultipleProject("OK");
  performanceLog("E","2","Check in multiple project");
  
}

function WaitStatusGrid()
{
  
}

function WaitAndCheckStatusGrid(CheckValue)
{
  var GridviewcellCheckOutBy = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.vwCSProjectList.Grid.RadGridViewProjects.GridViewRow.GridviewcellCheckOutBy;
  var i = 0;
	 
  while (GridviewcellCheckOutBy.WPFControlText != CheckValue)
  {
    	i++;
			aqUtils.Delay(500);
  		RefreshProjects();  
  }
  if(GridviewcellCheckOutBy.WPFControlText == CheckValue)
  {
    return true;
  }
  else
  {
    return false;
  }
  
}

function WaitProject(strvalue)
{
  Aliases.TrussStudio.FormTS.Item.wndAfx.AfxMDIFrame140u.page32770.BCGPTabWnd2c000081000310.Click(86, 240);
}

function Test1()
{
 Aliases.CSDirector.HwndSource_ChangeTaskStatusDialog.ChangeTaskStatusDialog.RadGridViewTasks.GridViewRow2.Gridviewcell2.DblClick(67, 5);
}