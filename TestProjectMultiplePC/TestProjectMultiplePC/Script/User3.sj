﻿//USEUNIT EditProject
//USEUNIT UtilitiesForApps
function Main ()
{
  performanceLog("S","3");
    LoginDirector("ldau1","ldau1");   
    //SaveExcelFile("2","3","x");
    performanceLog("E","3","Log In");
    User3();
}

function User3()
{
	var ProjectInGird		=		Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.vwCSProjectList.Grid.RadGridViewProjects.GridViewCell;
	var Component				= 	Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.RadTreeMenu.RadtreeviewitemComponents;
  var GridViewRow = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.vwCSProjectList.Grid.RadGridViewProjects.GridViewRow;
//	var Engineering_Tab	=		Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.EngineeringTab;
//	var Engineering_Grid=		Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.RadGridEngineering;
//	var UnsealedReport	=		Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonEngineeringReports.RadRibbonSplitButton;
	
	
	// Test Step 
		//LoginDirector("ldau1","ldau1");
		aqUtils.Delay(60000);
		// Step 4 Check out project Test7, Test8
		// Check out project Test7
		FindProject("Test7");
		GridViewRow.Click();
		performanceLog("S","3");
		CheckOutProject("Checked","ForEdit","OK")
		performanceLog("E","3","Check out project Test7");
		// Check out project Test8
		FindProject("Test8");
		GridViewRow.Click();
		performanceLog("S","3");
		CheckOutProject("Checked","ForEdit","OK")
		performanceLog("E","3","Check out project Test8");
	
		// 12. Find Project
		//aqUtils.Delay(300000);
		WaitToRun(1,"User2","step11");
		performanceLog("S","3");
    RefreshProjects();
    FindProject("Project1");
		performanceLog("E","3","Find Project");
		
		// 13. Check out project
  	GridViewRow.Click();
		performanceLog("S","3");
		CheckOutProject("Check","ForEdit","OK");
		performanceLog("E","3","Check Out Project 1");
		WriteTextToFile(Project.Path+"User3.txt","step13",0,0)
		aqUtils.Delay(10000);
		
		// 14. Print Unsealed Engineering
		performanceLog("S","3");
		ProjectInGird.DblClick();
		Component.Click();
		aqUtils.Delay(2000);
        
    
		// Click Engineering tab
		Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.componentsTab.EngineeringTab.Click();
		//Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.EngineeringTab.Click();
		// Select all
		//Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.RadGridEngineering.Keys("^r^a");
		Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.componentsTab.RadGridEngineering.Keys("^r^a");
		// Print and check print success
		performanceLog("S","3","Print Unsealed Engineering");
		Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonEngineeringReports.RadRibbonSplitButton.Click();
		aqUtils.Delay(2000);
		while(Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonEngineeringReports.RadRibbonSplitButton.Enabled == false)
		{
		}
		
		performanceLog("E","3","Print Unsealed Engineering");
		// Write to excel file status
		WriteTextToFile(Project.Path+"All.txt","e",0,0);	
			
		Aliases.CSDirector.HwndSource_CSCommandApplication.SetFocus();
    var ListviewitemProjectList = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.LvwOutlookMenu.ListviewitemProjectList;
      ListviewitemProjectList.Click();
		// 15. Checkin multiple user same time with other user
    WaitToRun(0,"All","e"); 
		performanceLog("S","3");
		CheckInMultipleProject("OK");
		performanceLog("E","3","Checkin multiple");
}

function test ()
{
	WaitToRun(2,"User2","step12");
}

