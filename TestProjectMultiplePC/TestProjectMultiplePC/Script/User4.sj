﻿//USEUNIT UtilitiesForApps 
//USEUNIT EditProject
var FormMain = UserForms.FormTS_AUTOMATION;

function Main ()
{
    performanceLog("S","4");
    LoginDirector("thanguyen","thanguyen");
    performanceLog("E","4","Log In");
    RunMain();
}


function RunMain()
{
  var GridViewRow = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.vwCSProjectList.Grid.RadGridViewProjects.GridViewRow;
  var RadtreeviewitemT02  = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer.RadPaneGroup.ComponentMainTreeView.RadTreeViewItem.RadtreeviewitemT02;
  var RadtreeviewitemComponents = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.RadTreeMenu.RadtreeviewitemComponents;
  var HomeRibbonTab = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.HomeRibbonTab;
  
  var Edit = Aliases.TrussStudio.FormTS.Item.wndAfx.AfxMDIFrame140u.page327702.Edit;
  var btnRun = Aliases.TrussStudio.FormTS.Item.wndAfx.AfxMDIFrame140u.page327702.btnRun;
  
	// Clear filter
  RefreshProjects();
  ClearFilter();
	
	// 12. Filter Project #component <100
  
	performanceLog("S","4");
  var CSDirector;
  CSDirector = Aliases.CSDirector;
  CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonProjectListOnly.btnProjectFilter.Click(31, 60);
  CSDirector.HwndSource_PopupRoot.PopupRoot.RadContextMenu.Radmenuitem1OfComponent100.Click(72, 15);
  performanceLog("E","4","Check time filler project"); 
	
	//13. Check out project for view
	WaitToRun(1,"User2","step11");
	//WaitToRun(2,"User2","step12");
  FindProject("Project1");
  
  GridViewRow.Click();
  performanceLog("S","4");
  CheckOutProject("Checked","ForView","OK");
  performanceLog("E","4","Check Out Project 1"); 
  
	//15 Check out large project SCG
  WaitToRun(0,"All","e");
  FindProject("SCG");
  GridViewRow.Click();
	
  performanceLog("S","4");
  CheckOutLargeProject("Checked","ForEdit","OK");
  performanceLog("E","4","Check Out SCG Project");
  
}


function WaitAndCheckStatusGrid(CheckValue)
{
  var GridviewcellCheckOutBy = Aliases.CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.radDocking.RadSplitContainer1.WorkspacePaneGroup.vwCSProjectList.Grid.RadGridViewProjects.GridViewRow.GridviewcellCheckOutBy;
    
  while (GridviewcellCheckOutBy.WPFControlText != CheckValue)
  {
    
  }
  if(GridviewcellCheckOutBy.WPFControlText == CheckValue)
  {
    return true;
  }
  else
  {
    return false;
  }
  
}

function Test1()
{
  Aliases.TrussStudio.FormTS.Item.wndAfx.AfxMDIFrame140u.page32770.BCGPTabWnd2c000081000310.Click(86, 240);
}

function ClearFilter()
{
  var CSDirector;
  CSDirector = Aliases.CSDirector;
  CSDirector.HwndSource_CSCommandApplication.CSCommandApplication.RadRibbonViewMain.RibbonProjectListOnly.btnProjectFilter.Click(23, 37);
  CSDirector.HwndSource_PopupRoot.PopupRoot.RadContextMenu.RadmenuitemClearFilter.Click(42, 19);
}

