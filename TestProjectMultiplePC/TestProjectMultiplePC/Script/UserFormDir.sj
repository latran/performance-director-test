﻿//USEUNIT UtilitiesForApps
//USEUNIT MainScript
//USEUNIT MainScriptDisable
//USEUNIT FiveUse_SameLogin


var FormMain = UserForms.FormTS_AUTOMATION;
var FormMassage = UserForms.FormMessage;

function Main()
{

  try
  {
    FormMain.ShowModal();
//    if (FormMain.ModalResult==mrCancel)
//     Log.Message("ModalResult = Cancel");
  }
  catch(exception)
  {
    Log.Error("Exception", exception.description);
  }
  
}

function FormTS_AUTOMATION_cxButtonEdit1_OnClick(Sender)
{
  if(FormMain.SelectDirectory1.Execute())
    FormMain.cxButtonEdit1.Text= UtilitiesForApps.EditPart(FormMain.SelectDirectory1.Directory);
}

function FormTS_AUTOMATION_cxButtonRun_OnClick(Sender)
{
  Project.Variables.FilePartDirector=UtilitiesForApps.EditPart(FormMain.cxButtonEdit1.Text);
  UtilitiesForApps.CloseApp();
  UtilitiesForApps.OpenApp();

  var StrMachine = FormMain.cboMachine.Text;
  
  FiveUse_SameLogin.Main(StrMachine);

  
  //MainScript.Main();
  //MainScriptDisable.Main();
  //MainScript002.Main();
  //MainScript003.Main();
  //MainScript004.Main();
   //MainScript005.Main();
  
//  switch (FormMain.cxMRUEdit1.Text)
//  {
//    case "001 - Hip Set : Cal Hip Mods to Hip Set Dialog" :
//      Project.Variables.FlagRun001=true;
//    break;
//    case "002 - Hip Set : Setback - Number of Plies - EndJack 1st - SideJack 1st" :
//      Project.Variables.FlagRun002=true;
//    break;
//    case "003 - Hip Set : HipTrussExt - EndJack Bevels - SideJack Bevels - Corner Bevels" :
//      Project.Variables.FlagRun003=true;
//    break;
//    case "004 - Hip Set : SecondaryHips - Full Locate Hip Girder - Full Force Common" :
//      Project.Variables.FlagRun004=true;
//    break;
//    case "005 - Hip Set : Corner Girder Style - End Jack Layout - Side Jack Layout" :
//      Project.Variables.FlagRun005=true;
//    break;
//    case "013 - Hip StepDown : Girder SetBack - Girder Plies - Corner Girder Plies - EndJack Layout - SideJack Layout" :
//      Project.Variables.FlagRun013=true;
//    break;
//    case "014 - Hip StepDown : Corner Girder Style - Dimension EndJacks - Dimension SideJacks" :
//      Project.Variables.FlagRun014=true;
//    break;
//    case "015 - Hip StepDown : Secondary Hips - Dimension EndJacks - Dimension SideJacks" :
//      Project.Variables.FlagRun015=true;
//    case "006 - Hip Set : Cal Hip Step Up" :
//      Project.Variables.FlagRun006=true;
//    case "017 - Hip StepDown : EndJack Bevels - SideJack Bevels - Corner Jacks" :
//      Project.Variables.FlagRun017=true;
//    case "016 - Hip StepDown : Hip Drop Dimension - Hip Drop Pitch" :
//      Project.Variables.FlagRun016=true;
//    case "029 - Hip : Single/Multiple - Common/FlatTop Truss" :
//      Project.Variables.FlagRun029=true;  
//      
//    break;
//    default :
//    
//    break;
//  }
//  
//  if (Project.Variables.FilePartTrussStudio != "")
//  {
//    MainScript001.Main001();
//    MainScript002.Main002();
//    MainScript003.Main003();
//    MainScript004.Main004();
//    MainScript005.Main005();
//    MainScript013.Main013();
//    MainScript014.Main014();
//    MainScript015.Main015();
//    MainScript006.Main006();
//    MainScript017.Main017();
//    MainScript016.Main016();
//    MainScript029.Main029();
//  }

}
function DisFlag()
{
//  Project.Variables.FlagRun001=false;
//  Project.Variables.FlagRun002=false;
//  Project.Variables.FlagRun003=false;
//  Project.Variables.FlagRun004=false;
//  Project.Variables.FlagRun005=false;
//  Project.Variables.FlagRun013=false;
//  Project.Variables.FlagRun014=false;
//  Project.Variables.FlagRun015=false;
//  Project.Variables.FlagRun006=false;
//  Project.Variables.FlagRun017=false;
//  Project.Variables.FlagRun016=false;
//  Project.Variables.FlagRun029=false;
}



//** Test *****

function FormTS_AUTOMATION_cxButton1_OnClick(Sender)
{
//FormMain.SelectDirectory1.Execute();
  FormMain.OpenDialog1.Execute();
  FormMain.cxButtonEdit1.Text = FormMain.OpenDialog1.FileName;
}

function FormTS_AUTOMATION_cxMRUEdit1_OnChange(Sender)
{
  Log.Message(FormMain.cxMRUEdit1.ImeName);
  Log.Message(FormMain.cxMRUEdit1.Text); 

}

function FormTS_AUTOMATION_OnHide(Sender)
{
  DisFlag();
}

function FormTS_AUTOMATION_OnShow(Sender)
{
  FormMain.cxButtonEdit1.Text="C:\\SST\\Client\\Director";
  FormMain.cboMachine.Properties.Items.Add("1");
  FormMain.cboMachine.Properties.Items.Add("2");
  FormMain.cboMachine.Properties.Items.Add("3");
	FormMain.cboMachine.Properties.Items.Add("4");
}

function MessageUSF ()
{
  FormMassage.Show();
}


