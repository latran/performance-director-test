﻿//USEUNIT MainScript1
//USEUNIT UtilitiesForApps


var FormMain = UserForms.FormTS_AUTOMATION;

var ListFtdltxt = ODT.Classes.NewArray();

function Main()
{
  try
  {
    FormMain.ShowModal();
//    if (FormMain.ModalResult==mrCancel)
//     Log.Message("ModalResult = Cancel");
  }
  catch(exception)
  {
    Log.Error("Exception", exception.description);
  }
}

function FormTS_AUTOMATION_cxButtonEdit1_OnClick(Sender)
{
  if(FormMain.SelectDirectory1.Execute())
    FormMain.cxButtonEdit1.Text= UtilitiesForApps.EditPartTrussStudio(FormMain.SelectDirectory1.Directory);
}
function FormTS_AUTOMATION_cxButtonEdit2_OnClick(Sender)
{
//  var openDialog = new ActiveXObject("MSComDlg.CommonDialog"); 
//  openDialog.DialogTitle = "Open my files..."; 
//  openDialog.InitDir = "C:\\WINDOWS\\"; 
//  openDialog.Filter = "All files (*.*)|*.*|Programs (*.com,*.exe)|" +
//                      "*.com;*.exe|Text (*.txt,*.log)|*.txt;*.log"; 
//  openDialog.FilterIndex = 2; 
//  openDialog.Flags = 2621952; 
//  openDialog.MaxFileSize = 32000; 
   
  
 if  (FormMain.OpenDialog1.Execute())
{
  FormMain.cxButtonEdit2.Text = FormMain.OpenDialog1.FileName;
}
  
if (aqString.FindLast(FormMain.cxButtonEdit2.Text,".tdlLayout") != -1)
{
  Project.Variables.FilePathTdl= FormMain.cxButtonEdit2.Text;
  ListFtdltxt = MainScript.ReadFile();
    
  FormMain.cxLabel6.Caption = "The File have total "+ ListFtdltxt.Count+ " lines ." ;
  
   for ( i = 0 ; i<ListFtdltxt.Count ; i++)
    {
        if (aqString.GetLength(ListFtdltxt.items(i)) < 10)
        {
            FormMain.cxTextEdit2.Text = (i+1);
            break;
        }
    }
    FormMain.cxTextEdit3.Text = ListFtdltxt.Count;
  
}
else
{
  var i;
  //i = MkSet(mbYes, mbYesToAll, mbNo, mbCancel);
  i = MessageDlg("Please input tdlLayout File.", mtError, mbOK, 0);
  FormMain.cxButtonEdit2.Text="";
}
  
}
function FormTS_AUTOMATION_cxButtonRun_OnClick(Sender)
{
  //Project.Variables.FilePartDirector=UtilitiesForApps.EditPartTrussStudio(FormMain.cxButtonEdit1.Text);
  //Project.Variables.NumLine= FormMain.cxTextEdit1.Text;
  //Project.Variables.FilePathTdl= FormMain.cxButtonEdit2.Text;
  //Project.Variables.tbLineFrom = FormMain.cxTextEdit2.Text;
  //Project.Variables.tbLineTo = FormMain.cxTextEdit3.Text;
  
  UtilitiesForApps.CloseApp();
  UtilitiesForApps.OpenApp();

  MainScript.Main();

//  switch (FormMain.cxMRUEdit1.Text)
//  {
//    case "001 - Hip Set : Cal Hip Mods to Hip Set Dialog" :
//      Project.Variables.FlagRun001=true;
//    break;
//    case "002 - Hip Set : Setback - Number of Plies - EndJack 1st - SideJack 1st" :
//      Project.Variables.FlagRun002=true;
//    break;
//    case "003 - Hip Set : HipTrussExt - EndJack Bevels - SideJack Bevels - Corner Bevels" :
//      Project.Variables.FlagRun003=true;
//    break;
//    case "004 - Hip Set : SecondaryHips - Full Locate Hip Girder - Full Force Common" :
//      Project.Variables.FlagRun004=true;
//    break;
//    case "005 - Hip Set : Corner Girder Style - End Jack Layout - Side Jack Layout" :
//      Project.Variables.FlagRun005=true;
//    break;
//    case "013 - Hip StepDown : Girder SetBack - Girder Plies - Corner Girder Plies - EndJack Layout - SideJack Layout" :
//      Project.Variables.FlagRun013=true;
//    break;
//    case "014 - Hip StepDown : Corner Girder Style - Dimension EndJacks - Dimension SideJacks" :
//      Project.Variables.FlagRun014=true;
//    break;
//    case "015 - Hip StepDown : Secondary Hips - Dimension EndJacks - Dimension SideJacks" :
//      Project.Variables.FlagRun015=true;
//    case "006 - Hip Set : Cal Hip Step Up" :
//      Project.Variables.FlagRun006=true;
//    case "017 - Hip StepDown : EndJack Bevels - SideJack Bevels - Corner Jacks" :
//      Project.Variables.FlagRun017=true;
//    case "016 - Hip StepDown : Hip Drop Dimension - Hip Drop Pitch" :
//      Project.Variables.FlagRun016=true;
//    case "029 - Hip : Single/Multiple - Common/FlatTop Truss" :
//      Project.Variables.FlagRun029=true;  
//      
//    break;
//    default :
//    
//    break;
//  }
//  
//  if (Project.Variables.FilePartTrussStudio != "")
//  {
//    MainScript001.Main001();
//    MainScript002.Main002();
//    MainScript003.Main003();
//    MainScript004.Main004();
//    MainScript005.Main005();
//    MainScript013.Main013();
//    MainScript014.Main014();
//    MainScript015.Main015();
//    MainScript006.Main006();
//    MainScript017.Main017();
//    MainScript016.Main016();
//    MainScript029.Main029();
//  }

}
function DisFlag()
{
//  Project.Variables.FlagRun001=false;
//  Project.Variables.FlagRun002=false;
//  Project.Variables.FlagRun003=false;
//  Project.Variables.FlagRun004=false;
//  Project.Variables.FlagRun005=false;
//  Project.Variables.FlagRun013=false;
//  Project.Variables.FlagRun014=false;
//  Project.Variables.FlagRun015=false;
//  Project.Variables.FlagRun006=false;
//  Project.Variables.FlagRun017=false;
//  Project.Variables.FlagRun016=false;
//  Project.Variables.FlagRun029=false;
}



//** Test *****

function FormTS_AUTOMATION_cxButton1_OnClick(Sender)
{
//FormMain.SelectDirectory1.Execute();
  FormMain.OpenDialog1.Execute();
  FormMain.cxButtonEdit1.Text = FormMain.OpenDialog1.FileName;
}

function FormTS_AUTOMATION_cxMRUEdit1_OnChange(Sender)
{
  Log.Message(FormMain.cxMRUEdit1.ImeName);
  Log.Message(FormMain.cxMRUEdit1.Text); 

}

function FormTS_AUTOMATION_OnHide(Sender)
{
  DisFlag();
}

function FormTS_AUTOMATION_OnShow(Sender)
{
  FormMain.cxButtonEdit1.Text="C:\\SST\\Local\\Director";
}

