﻿function OpenApp()
{
    var App4Test = TestedApps.CSDirector ;

//  var SubPart = aqString.SubString(Project.Variables.FilePartTrussStudio,0,aqString.FindLast(Project.Variables.FilePartTrussStudio,"TrussStudio"));
//  TestedApps.ClrCK.Path= SubPart;
//  TestedApps.ClrCK.FileName="ClrCK.exe";
//  TestedApps.ClrCK.Run();
  
  App4Test.Path=Project.Variables.FilePartDirector;
  Log.Message(Project.Variables.FilePartDirector);
  App4Test.FileName="CSDirector.exe";
  App4Test.Run();
}
function CloseApp()
{
//  var TrussStudio = Sys.WaitProcess("TrussStudio");
//  if(TrussStudio.Exists)
//    Sys.Process("TrussStudio").Terminate(); 
    
  var CSDirector = Sys.WaitProcess("CSDirector");
  if(CSDirector.Exists)
    Sys.Process("CSDirector").Terminate(); 
    
//  var SSTAssert = Sys.WaitProcess("SSTAssert");
//  if(SSTAssert.Exists)
//    Sys.Process("SSTAssert").Terminate(); 
}
function OpenSSTAssert ()
{
    var App4Test = TestedApps.SST_Assert;
  var UserName = aqString.ToLower(Sys.UserName);
  App4Test.Path="C:\\Users\\"+UserName+"\\AppData\\Roaming\\Microsoft\\Windows\\Start Menu\\Programs\\Simpson Strong-Tie VietNam";
  //Log.Message(Project.Variables.FilePartDirector);
  App4Test.FileName="SST Assert.appref-ms";
  App4Test.Run();

}
function OpenToolConvertPDFtoTXT()
{
    var App4Test = TestedApps.Simpson;
  var UserName = aqString.ToLower(Sys.UserName);
  App4Test.Path="C:\\PDF2TXT";
  //Log.Message(Project.Variables.FilePartDirector);
  App4Test.FileName="Simpson.exe";
  App4Test.Run();
}

function EditPart (PartF)
{        
    var EditPart ="";
    var IntPart = aqString.FindLast(PartF,"CSDirector.exe");
    if (IntPart == -1)
    {
        if (aqString.FindLast(PartF,"Director")!=-1)
        EditPart = aqString.SubString(PartF,0,aqString.FindLast(PartF,"Director")+8);
        else EditPart="...";
    }
    else
        EditPart = aqString.SubString(PartF,0,IntPart-1);
        
    return EditPart ;
    //EditPart = "C:\SST\Local\TrussStudio" 
      
}

function MessagePicture (ObjSnap,NoteMessage,arg)
{
var attrBold = Log.CreateNewAttributes();
attrBold.Bold = true;
//attrBoldBlue.FontColor = clWhite;
    var Pict ;
    LLPlayer.MouseMove(0,0,1000);
    Pict=Utils.Picture;
    Pict= ObjSnap.Picture();//snapshot picture
    if (arg==1)
    Log.Picture(Pict,NoteMessage,null,300,attrBold);
    if (arg==0)
    Log.Picture(Pict,NoteMessage);
    //Pict.Stretch(1647,647)/*Resize the picture*/
    //Pict.SaveToFile(PathToSave);

}
function AtriBold ()
{
  var AtriB = Log.CreateNewAttributes();
  AtriB.Bold = true ;
  return AtriB;
}


function ClickBtntruss (arg)
{
  // **arg** 1 : yes // 2 : no // 0 : Cancel
  
  var p = Sys.Process("TrussStudio").WaitWindow("#32770", "Component Solutions™ Truss Studio", 1,3000);
  if (p.Exists)
  { 
    if (arg==1)
    Aliases.TrussStudio.dlgComponentSolutionsTrussStudio.btnYes.ClickButton();
    if (arg==2)
    Aliases.TrussStudio.dlgComponentSolutionsTrussStudio.btnNo.ClickButton();
    if (arg==0)
    Aliases.TrussStudio.dlgComponentSolutionsTrussStudio.btnCancel.ClickButton();    
  }
}
function ResizeForm ()
{
var FormMainDirec =  Aliases.CSDirector.HwndSource_CSCommandApplication ;
  //var FormTS = Aliases.TrussStudio.FormTS ;
  FormMainDirec.Maximize();
  
//  hwndSource = Aliases.CSDirector.HwndSource_CSCommandApplication;
//  radScheduleView = hwndSource.CSCommandApplication.gridMain.vwCSApplicationContainer.Grid.radDocking.RadSplitContainer1.WorkspacePaneGroup.GridWorkspaceContainer.vwCSCalendarsContainer.ScheduleGrid.scheduleview;
//  radScheduleView.Keys("~[ReleaseLast] ");
//  hwndSource.SystemMenu.Click("Restore");
  
  CSDirector = Aliases.CSDirector;
  hwndSource = CSDirector.HwndSource_CSCommandApplication;
  hwndSource.ClickR(35, 12);
  hwndSource.PopupMenu.Click("Restore");


  
  
  //Aliases.TrussStudio.FormTS.SystemMenu.Click("Restore");
  FormMainDirec.Position(0,0,1600,900);
}

function ResizeFormTS ()
{
//var FormMainDirec =  Aliases.CSDirector.HwndSource_CSCommandApplication ;
  var FormTS = Aliases.TrussStudio.FormTS ;
  FormTS.Maximize();
  
//  hwndSource = Aliases.CSDirector.HwndSource_CSCommandApplication;
//  radScheduleView = hwndSource.CSCommandApplication.gridMain.vwCSApplicationContainer.Grid.radDocking.RadSplitContainer1.WorkspacePaneGroup.GridWorkspaceContainer.vwCSCalendarsContainer.ScheduleGrid.scheduleview;
//  radScheduleView.Keys("~[ReleaseLast] ");
//  hwndSource.SystemMenu.Click("Restore");
  
  FormTS.SystemMenu.Click("Restore");
  FormTS.Position(0,0,1600,900);
}
function ResizeFormTruss ()
{
//var FormMainDirec =  Aliases.CSDirector.HwndSource_CSCommandApplication ;
  
  var FormTruss = Aliases.TrussStudio.FormTrussStudio;
  FormTruss.Maximize();
  
//  hwndSource = Aliases.CSDirector.HwndSource_CSCommandApplication;
//  radScheduleView = hwndSource.CSCommandApplication.gridMain.vwCSApplicationContainer.Grid.radDocking.RadSplitContainer1.WorkspacePaneGroup.GridWorkspaceContainer.vwCSCalendarsContainer.ScheduleGrid.scheduleview;
//  radScheduleView.Keys("~[ReleaseLast] ");
//  hwndSource.SystemMenu.Click("Restore");
  
  FormTruss.SystemMenu.Click("Restore");
  FormTruss.Position(0,0,1600,900);
}

function ResetPositionToolbar ()
{
LLPlayer.MouseMove(0,0,1000);
var FormTS = Aliases.TrussStudio.FormTS ;
FormTS.Keys("[Alt][Right][Right][Enter]t[Enter][Release]");
  ClickBtntruss(1);
  aqUtils.Delay(3000);
}
function ResetPositionToolbarTruss ()
{
LLPlayer.MouseMove(0,0,1000);
var FormTS = Aliases.TrussStudio.FormTrussStudio ;
FormTS.Keys("[Alt][Right][Right][Enter]t[Enter][Release]");
  ClickBtntruss(1);
  aqUtils.Delay(3000);
}
function checkObj (Objc) 
{
  if (Objc.wState==0)
       Objc.Click();
}
function UncheckObj (Objc) 
{
  if (Objc.wState==1)
       Objc.Click();
}

function test13()
{
var temp = Aliases.TrussStudio.FormTruss.MDIClient.wndAfx2.AfxMDIFrame120u.page32770.chkHoldPlate;

pic = Utils.Picture;
pic = temp.Picture();
pic.SaveToFile(Project.Path+"\\DataPic\\chkHoldPlate.png");

Log.Picture(pic);
  
}
function t435()
{
CheckObjCompPic("ChkWindApplyLeft",Aliases.TrussStudio.dlgWindLoadDialog.chkApplytoLeft);
    
} 

function CheckObjCompPic(NamePic,objSnap)
{

  var ObjPicBase = Utils.Picture;
  var ObjPicComp = Utils.Picture;
  LLPlayer.MouseMove(0,0,1000);
  ObjPicBase.LoadFromFile(Project.Path+"\\DataPic\\"+NamePic+".png");
  ObjPicComp = objSnap.Picture();
  
  if(!ObjPicBase.Compare(ObjPicComp))
  {
    Log.Message("Check");
    objSnap.Click();
  }
}
function UncheckObjCompPic(NamePic,objSnap)
{

  var ObjPicBase = Utils.Picture;
  var ObjPicComp = Utils.Picture;
  LLPlayer.MouseMove(0,0,1000);
  ObjPicBase.LoadFromFile(Project.Path+"\\DataPic\\"+NamePic+".png");
  ObjPicComp = objSnap.Picture();
  
  Log.Picture(ObjPicBase);
  Log.Picture(ObjPicComp);
  
  if(ObjPicBase.Compare(ObjPicComp))
  {
    Log.Message("Uncheck");
    objSnap.Click();
  }

  
}

function ClickViewZoom (arg)
{
// 0: Zoom all - 1: Zoom Window - 4:PlanView
  if (arg==0)
  {
  wndAfx = Aliases.TrussStudio.FormTS;
  wndAfx.Afx.Afx.ClickItem(16, true);
  wndAfx.Afx3.ClickItem("Zoom &All", false);
  }
  if (arg==1)
  { 
  wndAfx = Aliases.TrussStudio.FormTS;
  wndAfx.Afx.Afx.ClickItem(16, true);
  wndAfx.Afx3.ClickItem("Zoom &Window", false);
  }
  if (arg==4)
  {
  wndAfx = Aliases.TrussStudio.FormTS;
  wndAfx.Afx.Afx.ClickItem(16, true);
  wndAfx.Afx3.ClickItem("&Plan View", false);
  }
}

function ClickSnapPoint(arg)
{
 // 1:MidPoint - 2:Center
  if (arg==1)
  {
  wndAfx = Aliases.TrussStudio.FormTS;
  wndAfx.Afx.Afx.ClickItem(17, true);
  wndAfx.Afx3.CheckItem("Midpoint", false, false);
  }
  if (arg==2)
  {
  wndAfx = Aliases.TrussStudio.FormTS;
  wndAfx.Afx.Afx.ClickItem(17, true);
  wndAfx.Afx3.CheckItem("Center", false, false);
  }
}

function ClickDimension (arg)
{
  //0:Dim - 4:Spacing
  if (arg==0)
  {
  wndAfx = Aliases.TrussStudio.FormTS;
  wndAfx.Afx.Afx.ClickItem(12, true);
  wndAfx.Afx3.CheckItem("Dims/Labels", true, false);
  }
  if (arg==4)
  {
  wndAfx = Aliases.TrussStudio.FormTS;
  wndAfx.Afx.Afx.ClickItem(12, true);
  wndAfx.Afx3.CheckItem("Spacing Dimensions", true, false);
  }
}

function ClearAllSelect ()
{
  wndAfx = Aliases.TrussStudio.FormTS;
  wndAfx.Afx.Afx.ClickItem(3, true);
  wndAfx.Afx3.ClickItem("Clear All", false);
}

function WaitWin (Obj)
{
  var F = false ;
  var i =0
  while (F==false)
  {
    if (Obj.Exists)
    {
      F=true;
    }
    if (i>12)
      break;
    i++;
  } 
  return F;
}

function ReadDataFromFile(sPath)
{
  var Arr = ODT.Classes.NewArray();

  var myFile = aqFile.OpenTextFile(sPath, aqFile.faRead, aqFile.ctUTF8);
  
  while(! myFile.IsEndOfFile())
  {
    s = myFile.ReadLine();
    Arr.AddItem(s);
  }

  myFile.Close();
  
  return Arr;

}
function WriteTextToFile(sPath,str1,line,col)
{

//  if (aqFile.Exists(sPath))
//  {
//    aqFile.Delete(sPath);
//  }
//
//  aqFile.Create(sPath);

  var myFile = aqFile.OpenTextFile(sPath, aqFile.faWrite, aqFile.ctUTF8);

	myFile.SetPosition(line,col);
  myFile.WriteLine(str1);
  //myFile.WriteLine(str2);
  
  myFile.Close();
}

//********************Lan Tran 14/11/2019
// row col begin by 1
function SaveExcelFile(row,col,strValue)
{
    
  //Open Excel output file
  var app = Sys.OleObject("Excel.Application");
  var book = app.Workbooks.Open(Project.Path + "Database.xlsx");//"C:\\SAS LSD Data Driven-output - Copy"
  var sheet = book.Sheets("S1");
  app.DisplayAlerts = false;
  
  
  sheet.Cells(row, col) = strValue;
  
  
  book.Save()
  app.Quit();
}

// row col begin by 1
function ReadExcelFile (row,col,CheckValue)
{
  var driver = DDT.ExcelDriver( Project.Path + "Database.xlsx","S1",true);
  var a =0 ;
  //Log.Message(Project.Path + "Database.xlsx");
  row=row-2;
  col=col-1;
  
  while (!driver.EOF())
  { 
  
//  if (a>0) 
//      FReaddata =true;
//    else  
//      FReaddata =false;
       
//				Log.Message(driver.Value(col)); 
//				Log.Message(driver.Value(col-1));
//				Log.Message(driver.Value(col-2));
  if (driver.Value(0)!=null)
  {

//    if (FReaddata==true)
//    {
      if(a==row)
      {
//        Log.Message(driver.Value(col)); 
//				Log.Message(driver.Value(col-1));  
     
        if(driver.Value(col) == CheckValue)
        {
          return true;
        }
        else
        {
          return false;
        }
//      }
    }
  }

   
    DDT.CurrentDriver.Next();
    a++;
  }
  DDT.CloseDriver(driver.Name);
  
  return false;
}

function setLayoutCommand (strValue)
{
  editLayoutComm =  Aliases.TrussStudio.FormTrussStudio.MDIClient.wndAfx.AfxMDIFrame140u.page32770.editLayoutCommands;
  editLayoutCommands.SetText(strValue);
  editLayoutComm.Keys("[End][Enter][Release]");
  
}
function LoginDirector (user,pass)
{
  Aliases.CSDirector.HwndSource_winLoginDialog
  var editPassword = Aliases.CSDirector.HwndSource_winLoginDialog.winLoginDialog.Password;
  var editUsername = Aliases.CSDirector.HwndSource_winLoginDialog.winLoginDialog.Username;
  var BtnOk = Aliases.CSDirector.HwndSource_winLoginDialog.winLoginDialog.Okay;
  
  var Fsus = false;
  

    if (WaitWin(Aliases.CSDirector.HwndSource_winLoginDialog))
    {
      editUsername.SetText(user);
      editPassword.Keys(pass);
      BtnOk.Click();
      Fsus = true ;
    } 	
		while (!Aliases.CSDirector.HwndSource_CSCommandApplication.Exists)
		{
		}
  return Fsus;
}
//------ WaitToRun function -------//
function WaitToRun(row,FileName,ValueCheck)
{
	var i = 0;
	
	var Arr = ODT.Classes.NewArray();
//	Arr=ReadDataFromFile("S:\\@QA\\@AutomaionServer\\TestComplete\\TestProjectMultiplePC\\TestProjectMultiplePC\\"+FileName+".txt");
//	var a = Arr.Items(row);
	// Waiting for status update
	var a= "0";
				while (a != ValueCheck)
				{
					i++;	
					Arr=ReadDataFromFile(Project.Path+FileName+".txt");	
//					if( Arr.Count != 0)
//					{
						a = Arr.Items(row);
//					}
					aqUtils.Delay(1000);
					
				}
}

function testst()
{
  SaveDataReport("1","abc",21123421321);
}
function SaveDataReport(strUser,strDesc,strValue)
{
  var pathName="";
  switch ( strUser )
  {
    case "1":
        pathName = "User1Report";
      break;
  
    case "2" :
        pathName = "User2Report";
      break;
      
    case "3" :
        pathName = "User3Report";
      break;  

    case "4" :
        pathName = "User4Report";
      break;        
      
  }
  

  var myFile = aqFile.OpenTextFile(Project.Path+"ReportData\\"+pathName+".csv", aqFile.faWrite, aqFile.ctUTF8);

  Log.Message(myFile.LinesCount);
  myFile.SetPosition(myFile.LinesCount,0);
  myFile.WriteLine(strDesc+","+strValue);
  
  myFile.Close();
}